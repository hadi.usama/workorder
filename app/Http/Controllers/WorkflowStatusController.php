<?php

namespace App\Http\Controllers;

use App\Models\workflow_status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WorkflowStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\workflow_status  $workflow_status
     * @return \Illuminate\Http\Response
     */
    public function show(workflow_status $workflow_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\workflow_status  $workflow_status
     * @return \Illuminate\Http\Response
     */
    public function edit(workflow_status $workflow_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\workflow_status  $workflow_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, workflow_status $workflow_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\workflow_status  $workflow_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(workflow_status $workflow_status)
    {
        //
    }
}
