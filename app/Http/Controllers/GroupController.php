<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Wo_group_user;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$status = Status::find($id);
        $users = DB::table('user')->get();
        //print_r($users); exit;
        return view('group/group-add', compact(['users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //print_r($_POST); exit;
        $this->validate($request, [
            'group_name' => 'required|unique:groups|min:3',
            'group_users' => 'required',
        ], [],
        [
            'group_name' => 'Group Name',
            'group_users' => 'Group Users',
        ]);
        //print_r($_POST); exit;
        $group = new Group;
        $group->group_name           = $_POST["group_name"];
        $group->group_description    = $_POST["group_description"];
        $group->active_status        = 1;
        $save_group = $group->save();
        $group_id = $group->id;

        if($save_group==1){
            $users = array();
            //foreach($_POST["status"] as $index =>$item){
                foreach($_POST["group_users"] as $index =>$item){
                    $users[] = [
                        'group_id'      => $group_id,
                        'user_id'       => $item,
                        'created_at'    => \Carbon\Carbon::now(),
                        'updated_at'    => \Carbon\Carbon::now(),
                    ];
                }
        }
        DB::table('wo_group_users')->insert($users);

        return redirect('group-list')->with('record', 'Group Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        $users = DB::table('user')->get();
        $group_users = DB::table('wo_group_users')->where('group_id', $id)->get()->toArray();
        $results = array_column($group_users, 'user_id');
        //print_r($results); exit;
        return view('group/group-edit', compact(['group','users','results']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'group_name' => 'required|min:3',
        ], [],
        [
            'group_name' => 'Group Name',
        ]);
        $group = Group::find($_POST["id"]);

        $group->group_name           = $_POST["group_name"];
        $group->group_description    = $_POST["group_description"];
        $group->active_status = 1;

        $save_group = $group->save();
        $group_id = $group->id;
        if($save_group==1){
            $deletedRows = DB::table('wo_group_users')->where('group_id', $_POST['id'])->delete();
            $users = array();
            foreach($_POST["group_users"] as $index =>$item){
                $users[] = [
                    'group_id'      => $group_id,
                    'user_id'       => $item,
                    'created_at'    => \Carbon\Carbon::now(),
                    'updated_at'    => \Carbon\Carbon::now(),
                ];
            }
        }
        DB::table('wo_group_users')->insert($users);
        return redirect('group-list')->with('record', 'Status Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //
    }
    public function ajaxGroupList()
    {
        //$user = auth()->user()->id;
        $group = Group::all();
        //print_r($status); exit;
        return Datatables::of($group)
        ->editColumn('active_status', function($group) {
            if($group->active_status==1){
                return '<span class="badge badge-primary">Active</span>';
            }else {
                return '<span class="badge badge-danger">Inactive</span>';
            }
        })
        ->addColumn('action', function ($group) {
            // if($ticket->booking_status == 0)
            // {
                return "<div style=\"color:white\">
                            <a data-id='.$group->id.' href=\"/edit-group/".$group->id."\" class=\"btn btn-success detail-order\" type=\"button\" >Update</a>
                        <div>";
            // <a data-id='.$group->id.' href=\"#\" class=\"btn btn-danger start-process\">Delete</a>
            // }
            // else{
            // return '<div style="color:white">
            //             <a data-id='.$ticket->id.' class="btn btn-primary detail-order" type="button" data-toggle="modal" data-target="#exampleModalCenter" >Detail</a>
            //             <a data-id='.$ticket->id.' href="#" class="btn btn-success active-service">Approved
            //             </a>
            //         <div>';
            // // return 'status';
            // }
        })
        ->rawColumns(['active_status','action'])
        ->make(true);
    }
    public function ajaxGroupsRecords()
    {

        //$user = auth()->user()->id;
        $group = Group::all();
        //print_r($status); exit;
        return json_encode($group, true);
    }
}
