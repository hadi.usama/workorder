<?php

namespace App\Http\Controllers;

use App\Models\workflow;
use App\Models\workflow_status;
use App\Models\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;

class WorkflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::where('active_status',1)->get()->toArray();
        //print_r($status); exit;
        $status_name = array_column($status, 'name');
        $status_id = json_encode(array_column($status, 'id'),true);
        //print_r($status_id); exit;
        return view('workflow-engine/workflow-engine-add', compact('status','status_name','status_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:workflows|min:3',
            'description' => 'required|min:3',
        ], [],
        [
            'name' => 'Workflow Name',
            'description' => 'Description',
        ]);
        //
        // /print_r($_POST); exit;
        $workflow = new workflow;
        $workflow->name           = $_POST["name"];
        $workflow->description    = $_POST["description"];
        $save_status = $workflow->save();
        //$save_status = 1;
        $workflow_id = $workflow->id;
        if (!(array_key_exists(0, $_POST['status']))) {
            $fields_id = json_decode($_POST['fields']);
            //print_r($fields_id); exit;
            foreach($fields_id as $key => $index){
                $_POST["status"][0][$index]=1;
            }
            //print_r($_POST); exit;
        }
        //print_r($workflow_id); exit;
        if($save_status==1){
            //print_r($_POST["status"]);
            // /$workflow_status = new workflow_status;
            $book_records = array();
            foreach($_POST["status"] as $index =>$item){
                foreach($item as $index1 =>$item1){
                    $book_records[] = [
                        'workflow_id'   => $workflow_id,
                        'from_status'   => $index,
                        'to_status'     => $index1,
                        'value'         => $item1,
                        'created_at'    => \Carbon\Carbon::now(),
                        'updated_at'    => \Carbon\Carbon::now(),
                    ];
                }
            }
            workflow_status::insert($book_records);
        };
        return redirect('workflow-list')->with('record', 'Workflow Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function show(workflow $workflow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //print_r($id); exit;
        $workflow = workflow::find($id);
        $workflow_status = workflow_status::where('workflow_id',$id)->get()->toArray();
        $status = Status::where('active_status',1)->get()->toArray();
      //  echo '<pre>';
       // print_r($workflow_status); exit;
        foreach($workflow_status as $k=>$v){
            $selected[$v['from_status']][$v['to_status']]=1;
        }

        //print_r($selected); exit;
        $status_name = array_column($status, 'name');


        //print_r($workflow_status); exit;
        return view('workflow-engine/workflow-engine-edit', compact(['workflow','status','status_name','workflow_status','selected']));
        //echo "hiii"; exit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, workflow $workflow)
    {
        //print_r($_POST); exit;
        $workflow = workflow::find($_POST['id']);
        //print_r($workflow); exit;
        $workflow->name             =   $_POST['name'];
        $workflow->description      =   $_POST['description'];
        $worflow_return = $workflow->save();
        //print_r($worflow_return); exit;
        if($worflow_return==1)
            $deletedRows = workflow_status::where('workflow_id', $_POST['id'])->delete();
            if($deletedRows){
                $workflow_status = new workflow_status;
                $book_records = array();
                foreach($_POST["status"] as $index =>$item){
                    foreach($item as $index1 =>$item1){
                        $workflow_status->workflow_id     = $_POST['id'];
                        $workflow_status->from_status     = $index;
                        $workflow_status->to_status       = $index1;
                        $workflow_status->value           = $item1;
                        $book_records[] = [
                            'workflow_id'   => $_POST['id'],
                            'from_status'   => $index,
                            'to_status'     => $index1,
                            'value'         => $item1,
                            'created_at'    => \Carbon\Carbon::now(),
                            'updated_at'    => \Carbon\Carbon::now(),
                        ];
                    }
                }
                workflow_status::insert($book_records);
            }
        return redirect('workflow-list')->with('record', 'Workflow Updated!');
        // $workflow_status = workflow_status::where('workflow_id', $_POST['id'])->get()->toArray();
        // foreach($workflow_status as $k=>$v){
        //     $selected[$v['from_status']][$v['to_status']]=1;
        // }
        // print_r($selected);
        // print_r($_POST);
        // exit;
        //print_r($_POST); exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(workflow $workflow)
    {
        //
    }
    public function ajaxWorkflowList()
    {
        //$user = auth()->user()->id;
        $workflow = workflow::all();
        //print_r($status); exit;
        return Datatables::of($workflow)

        ->editColumn('is_active', function($workflow) {

            if($workflow->is_active==1){
                return '<span class="badge badge-primary">Active</span>';
            }else {
                return '<span class="badge badge-danger">Inactive</span>';
            }
        })
        ->addColumn('action', function ($workflow) {
            // if($ticket->booking_status == 0)
            // {
                return "<div style=\"color:white\">
                            <a data-id='.$workflow->id.' href=\"/edit-workflow/".$workflow->id."\" class=\"btn btn-success detail-order\" type=\"button\" >Update</a>
                        <div>";
                        //<a data-id='.$workflow->id.' href=\"#\" class=\"btn btn-danger start-process\">Delete</a>
            // }
            // else{
            // return '<div style="color:white">
            //             <a data-id='.$ticket->id.' class="btn btn-primary detail-order" type="button" data-toggle="modal" data-target="#exampleModalCenter" >Detail</a>
            //             <a data-id='.$ticket->id.' href="#" class="btn btn-success active-service">Approved
            //             </a>
            //         <div>';
            // // return 'status';
            // }
        })
        ->rawColumns(['is_active','action'])
        ->make(true);
    }
}
