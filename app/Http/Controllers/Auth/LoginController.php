<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\DB;
use App\Models\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // public function login(Request $request)
    // {
    //     $user = DB::table('user')->where('email', $request->email)->get();
    //     //dd($user[0]->email);
    //     if($user->count()) {
    //         $user = $user->first();
    //         if(md5($request->password) == $user->password) {
    //             if (Auth::attempt(['email' => 'admin@admin.com', 'password' => 'admin123'], $request->get('remember'))) {
    //                 $user = (array) $user;
    //                 $request->session()->put($user);
    //                 return redirect()->intended('/home');
    //             }
    //         }
    //         //Auth::login($user);
    //         return back()->withInput($request->only('email', 'remember'));
    //     }
    // }
    // protected function attemptLogin(Request $request)
    // {
    //     $user = \App\Models\User::where([
    //         'email' => $request->email,
    //         'password' => md5($request->password)
    //     ])->first();
    //     dd($user);
    //     if ($user) {
    //         $this->guard()->login($user, $request->has('remember'));

    //         return true;
    //     }

    //     return false;
    // }
    public function login(Request $request) {
        $user = User::where('email', $request->email) ->where('password',md5($request->password)) ->first();
        if($user){
            Auth::login($user);
            //print_r(Auth::user()['username']); exit;
            return redirect()->intended('/workorder-list');
        }else{
            return back()->with('message', 'Email and/or password invalid.');
            // echo "hi";
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
    public function loginRedirect(){
        return redirect('/');
    }
}
