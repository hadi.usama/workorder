<?php

namespace App\Http\Controllers;

use App\Models\workorder;
use App\Models\Status;
use App\Models\GroupApproval;
use App\Models\Group;
use App\Models\Workorder_template;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Illuminate\Support\Facades\Auth;

class GroupApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Status::where('active_status',1)->get()->toArray('id','name');
        $templates = Workorder_template::get()->toArray('id','name');
        $offices = DB::table('om_office')->select('id','name')->get();
        //print_r($offices); exit;
        return view('group-approval/group-approval-list', compact('status','templates','offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupApproval  $groupApproval
     * @return \Illuminate\Http\Response
     */
    public function show(GroupApproval $groupApproval)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GroupApproval  $groupApproval
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workorder  =   DB::table('workorders')
                        ->leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
                        ->leftJoin('user as u1', 'workorders.issuer_id', '=', 'u1.id')
                        ->leftJoin('om_office as office', 'workorders.issuer_office_id', '=', 'office.id')
                        ->leftJoin('user as u2', 'workorders.approver_id', '=', 'u2.id')
                        ->leftJoin('workorder_templates', 'workorders.template_id', '=', 'workorder_templates.id')
                        ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
                        ->leftJoin('user as u3', 'workorders.updated_by', '=', 'u3.id')
                        ->where('workorders.id', '=', $id)
                        ->first(['workorders.*','u1.first_name as issuer_name', 'statuses.name as status_name',
                        'workorder_templates.name as template_name','om_office.name as office_name'
                        ,'office.name as office_name','u2.first_name as approver_name','u3.first_name as updated_name']);
        $workorder = (array) $workorder;
        //print_r($workorder); exit;
        $status = Status::where('active_status',1)->get()->toArray();
        $offices = DB::table('om_office')->get();

        $status_name = array_column($status, 'name');
        $child  =   workorder::leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
                    ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
                    ->where('parent_id',$id)
                    ->get(['workorders.*', 'statuses.name as status_name','om_office.name as office_name'])->toArray();
        $group_members = GroupApproval::leftJoin('user', 'group_approvals.user_id', '=', 'user.id')
        ->where('wo_id',$id)
        ->get(['user.first_name','user.mobile_no','group_approvals.approval_status','group_approvals.user_id'])->toArray();
        //print_r($group_members); exit;
        return view('group-approval/group-approval-edit', compact('workorder','status','offices','child','group_members'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GroupApproval  $groupApproval
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupApproval $groupApproval)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GroupApproval  $groupApproval
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupApproval $groupApproval)
    {
        //
    }
    public function ajaxGroupWorkorderList()
    {
        $user_groups =      DB::table('wo_group_users')
                            ->select('group_id')
                            ->where('user_id',Auth::user()->id)
                            ->groupBy('group_id')
                            ->get()->toArray();
                        //$user_groups = (array) $user_groups;
        $groups=array();
        foreach($user_groups as $key => $value){
            $groups[] = $value->group_id;
        };

        $data_filters = $_POST['data_filters'];
        parse_str($data_filters,$data_filter);
        //print_r($data_filter); exit;
        //DB::enableQueryLog();
        //$user = auth()->user()->id;
        $workorder = DB::table('workorders')
        ->leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
        ->leftJoin('workorder_templates', 'workorders.template_id', '=', 'workorder_templates.id')
        ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
        // ->leftJoin('group_approvals', 'workorders.group_approval_required', '=', 'group_approvals.wo_id')
        // ->where(function ($query) {
        //     $query  ->where('line_manager_approval_required','=','1')
        //             ->where('workorders.approver_id','=',NULL)
        //             ->where('issuer_office_id','=',Auth::user()->om_office_id);
        // })->orWhere(function($query)use ($groups) {
        //     $query  ->where('group_approvals.user_id', Auth::user()->id)
        //             ->where('group_approvals.approval_status','=',"Pending");
        // })
        ->where(function ($query) use($data_filter){
            foreach($data_filter['integer'] as $key => $value){
                if($value!= ""  && isset($value)){
                    $query->where(function ($query) use($value,$key){
                        $query->where($key,$value);
                    });
                }
            };
            foreach($data_filter['string'] as $key => $value){
                if($value!= ""  && isset($value)){
                    $query->where(function ($query) use($value,$key){
                        $query->where($key,'Like','%'.$value.'%');
                    });
                }
            };
        //exit;
        })

        ->where(function ($query){
            $query->where(function ($query) {
                    $query ->where('line_manager_approval_required', 1)
                        ->where('workorders.approver_id',NULL);
                })->orWhere(function($query){
                    $query  ->where('group_approval_required','!=', NULL)
                            ->where('group_approval_status',0);
                });
        })

        // ->where('line_manager_approval_required','=','1')
        // //->where('group_approval_required','=','1')
        // ->where('approver_id','=',NULL)
        // ->where('issuer_office_id','=',Auth::user()->om_office_id)
        // ->orWhereIn('group_approval_required', $groups)
        // ->where('group_approval_status','=',0)
        ->get(['workorders.*', 'statuses.name as status_name','workorder_templates.name as template_name','om_office.name as office_name']);
        //dd(DB::getQueryLog());
        // print_r($workorder); exit;
        return Datatables::of($workorder)

        ->editColumn('priority', function($workorder) {

            if($workorder->priority==1){
                return "<span class=\"badge badge-primary\">High</span>";
            }elseif($workorder->priority==2) {
                return "<span class=\"badge badge-primary\">Medium</span>";
            }elseif($workorder->priority==3) {
                return "<span class=\"badge badge-primary\">Low</span>";
            }else {
                return "<span class=\"badge badge-danger\">Normal</span>";
            }
        })
        ->editColumn('vendor', function($workorder) {

            if($workorder->vendor==1){
                return "<span class=\"badge badge-primary\">Huawei</span>";
            }elseif($workorder->vendor==2) {
                return "<span class=\"badge badge-primary\">ZTE</span>";
            }elseif($workorder->vendor==3) {
                return "<span class=\"badge badge-primary\">Nokia</span>";
            }
        })
        ->editColumn('domain', function($workorder) {

            if($workorder->domain==1){
                return "<span class=\"badge badge-primary\">RAN</span>";
            }elseif($workorder->domain==2) {
                return "<span class=\"badge badge-primary\">Core</span>";
            }elseif($workorder->domain==3) {
                return "<span class=\"badge badge-primary\">Optical</span>";
            }elseif($workorder->domain==4) {
                return "<span class=\"badge badge-primary\">Microwave</span>";
            }elseif($workorder->domain==5) {
                return "<span class=\"badge badge-primary\">Datacom</span>";
            }elseif($workorder->domain==6) {
                return "<span class=\"badge badge-primary\">Power</span>";
            }
        })
        ->editColumn('id', function($workorder) {

            return "<a class=\"text-success font-weight-bold font-italic\" href=\"/edit-group-workorder/$workorder->id \" >$workorder->id</a>";
        })
        ->rawColumns(['is_active','id','priority','vendor','domain'])
        ->make(true);
    }
    public function user_approval(){
        //print_r($_POST); exit;
        GroupApproval::where('wo_id', $_POST['wo_id'])
        ->where('user_id', $_POST['user_id'])
        ->update(['approval_status' => 'Approved','approver_id'=>Auth::user()->id,'note'=> $_POST['note']]);

        $counts_pending = GroupApproval::where('wo_id',$_POST['wo_id'])
        ->where('approval_status','Pending')
        ->get() ->count();
        if($counts_pending == 0 ){
            workorder::where('id',$_POST['wo_id'])
            ->update(['group_approval_status' => 1]);
        }
        //print_r($counts_pending); exit;

        return redirect()->route('group.workorder.edit', ['id' => $_POST["wo_id"]]);
    }
    public function manager_approval(){
        workorder::where('id',$_POST['wo_id'])
        ->update(['approver_id' => Auth::user()->id]);
        return redirect()->route('group.workorder.edit', ['id' => $_POST["wo_id"]]);
    }
}
