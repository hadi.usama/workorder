<?php

namespace App\Http\Controllers;

use App\Models\Wo_office;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WoOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wo_office  $wo_office
     * @return \Illuminate\Http\Response
     */
    public function show(Wo_office $wo_office)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wo_office  $wo_office
     * @return \Illuminate\Http\Response
     */
    public function edit(Wo_office $wo_office)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wo_office  $wo_office
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wo_office $wo_office)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wo_office  $wo_office
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wo_office $wo_office)
    {
        //
    }
}
