<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;
use Datatables;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:statuses|min:3',
            'status_type' => 'required',
            'active_status' => 'required',
        ], [],
        [
            'name' => 'Status Name',
            'status_type' => 'Status Type',
            'active_status' => 'Active Status',
        ]);
        //print_r($_POST); exit;
        $status = new Status;
        $status->name           = $_POST["name"];
        $status->description    = $_POST["description"];
        $status->status_type    = $_POST["status_type"];
        $status->active_status  = $_POST["active_status"];
        $status->save();

        return redirect('status-list')->with('record', 'Status Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show(Status $status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //print_r($id); exit;
        $status = Status::find($id);
        return view('status/status-edit', compact(['status']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        // print_r($_POST); exit;
        $status = Status::find($_POST["id"]);

        $status->name           = $_POST["name"];
        $status->description    = $_POST["description"];
        $status->status_type    = $_POST["status_type"];
        $status->active_status  = $_POST["active_status"];

        $status->save();
        return redirect('status-list')->with('record', 'Email and/or password invalid.');
        // $user->email = 'john@foo.com';

        // $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        //
    }
    public function ajaxStatusList()
    {
        //$user = auth()->user()->id;
        $status = Status::all();
        //print_r($status); exit;
        return Datatables::of($status)
        ->editColumn('active_status', function($status) {
            if($status->active_status==1){
                return '<span class="badge badge-primary">Active</span>';
            }else {
                return '<span class="badge badge-danger">Inactive</span>';
            }
        })
        ->addColumn('action', function ($status) {
            // if($ticket->booking_status == 0)
            // {
                return "<div style=\"color:white\">
                            <a data-id='.$status->id.' href=\"/edit-status/".$status->id."\" class=\"btn btn-success detail-order\" type=\"button\" >Update</a>
                        <div>";
            //<a data-id='.$status->id.' href=\"#\" class=\"btn btn-danger start-process\">Delete</a>
            // }
            // else{
            // return '<div style="color:white">
            //             <a data-id='.$ticket->id.' class="btn btn-primary detail-order" type="button" data-toggle="modal" data-target="#exampleModalCenter" >Detail</a>
            //             <a data-id='.$ticket->id.' href="#" class="btn btn-success active-service">Approved
            //             </a>
            //         <div>';
            // // return 'status';
            // }
        })
        ->rawColumns(['active_status','action'])
        ->make(true);
    }
    public function ajaxStatusRecords()
    {

        //$user = auth()->user()->id;
        $status = Status::all();
        //print_r($status); exit;
        return json_encode($status, true);
    }
}
