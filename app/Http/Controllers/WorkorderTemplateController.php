<?php

namespace App\Http\Controllers;

use App\Models\Workorder_template;
use App\Http\Controllers\Controller;
use App\Models\workflow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Auth;

class WorkorderTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "hi";  exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $workflow = workflow::where('is_active',1)->get()->toArray();
        $offices = DB::table('om_office')->get();
        $groups = DB::table('groups')->get();
        return view('workorder-template/workorder-add', compact('workflow', 'offices','groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //print_r(Auth::user()['id']); exit;
        $Workorder_template = new Workorder_template;
        $Workorder_template->name                   = $_POST['name'];
        $Workorder_template->workflow_id            = $_POST['workflow_id'];
        $Workorder_template->approval_group_id      = ($_POST['cab_approval']!="")?$_POST['cab_approval']:NULL;
        $Workorder_template->line_manager_approval  = $_POST['manager_approval'];
        $Workorder_template->execution_start        = ($_POST['start_time']!="")?$_POST['start_time']:NULL;
        $Workorder_template->execution_end          = ($_POST['end_time']!="")?$_POST['end_time']:NULL;
        $Workorder_template->wo_fields_configs      = isset($_POST['fields']) ? json_encode($_POST['fields'],true) : json_encode(array(),true);
        $Workorder_template->created_by             = Auth::user()['id'];
        $Workorder_template->created_at             = \Carbon\Carbon::now();
        $Workorder_template->updated_at             = \Carbon\Carbon::now();
        $workorder_template_save = $Workorder_template->save();
        $Workorder_template_id = $Workorder_template->id;

        if($workorder_template_save==1){
            $groups = array();
            //foreach($_POST["status"] as $index =>$item){
                foreach($_POST["unit"] as $index =>$item){
                    $groups[] = [
                        'template_id'   => $Workorder_template_id,
                        'office_id'       => $item,
                        'created_at'    => \Carbon\Carbon::now(),
                        'updated_at'    => \Carbon\Carbon::now(),
                    ];
                }
        }
        DB::table('wo_offices')->insert($groups);
        return redirect('workorder-temp-list')->with('record', 'WO Template Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workorder_template  $workorder_template
     * @return \Illuminate\Http\Response
     */
    public function show(Workorder_template $workorder_template)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Workorder_template  $workorder_template
     * @return \Illuminate\Http\Response
     */
    public function edit(Workorder_template $workorder_template)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workorder_template  $workorder_template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workorder_template $workorder_template)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workorder_template  $workorder_template
     * @return \Illuminate\Http\tempResponse
     */
    public function destroy(Workorder_template $workorder_template)
    {
        //
    }
    public function ajaxWokorderTempList()
    {
        //$user = auth()->user()->id;
        $Workorder_template = DB::table('workorder_templates')
        ->leftJoin('user', 'workorder_templates.created_by', '=', 'user.id')
        ->get(['workorder_templates.*', 'user.first_name as user_name']);
        //print_r($Workorder_template); exit;
        return Datatables::of($Workorder_template)

        ->addColumn('action', function ($Workorder_template) {
                return "<div style=\"color:white\">
                            <a data-id='.$Workorder_template->id.'  class=\"btn btn-success detail-order disabled\" type=\"button\" >Go to WO Template</a>
                        <div>";
        })
        // href=\"/edit-template/".$Workorder_template->id."\"
        ->rawColumns(['action'])
        ->make(true);
    }
}
