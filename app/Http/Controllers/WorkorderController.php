<?php

namespace App\Http\Controllers;

use App\Models\workorder;
use App\Models\wo_file;
use App\Models\workflow;
use App\Models\workflow_status;
use App\Models\workflow_offices;
use App\Models\GroupApproval;
use App\Models\Status;
use App\Models\Wo_office;
use App\Models\Workorder_template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Auth;
use phpDocumentor\Reflection\Types\Null_;

class WorkorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Status::where('active_status',1)->get()->toArray('id','name');
        $templates = Workorder_template::get()->toArray('id','name');
        $offices = DB::table('om_office')->select('id','name')->get();
        //print_r($offices); exit;
        return view('workorder/workorder-list', compact('status','templates','offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //print_r(Auth::user()); exit;
        //$status = Status::where('active_status',1)->get()->toArray();
        //$workflow = workflow::where('is_active',1)->get()->toArray();
        //print_r($status); exit;
        //$status_name = array_column($status, 'name');
        //print_r($status); exit;
        // return view('workorder-template/workorder-add', compact('status','status_name'));
        $groups = DB::table('groups')->get();
        $templates = Workorder_template::get()->toArray();
        $status = Status::where('active_status',1)->get()->toArray();
        $offices = DB::table('om_office')->get();
        return view('workorder/workorder-add', compact('templates','status','offices','groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r($_POST); exit;
        $this->validate($request, [
            'subject' => 'required|min:3',
            'priority' => 'required',
            'template_id' => 'required',
            'status' => 'required',
        ], [],
        [
            'subject' => 'Workorder Subject',
            'priority' => 'Priority',
            'template_id' => 'Template',
            'status' => 'Status',
        ]);
        //print_r($_POST); exit;
        //
        $Workorder = new Workorder;
        $Workorder->template_id                     = $_POST['template_id'];
        $Workorder->subject                         = $_POST['subject'];
        $Workorder->description                     = $_POST['description'];
        $Workorder->status                          = $_POST['status'];
        $Workorder->priority                        = $_POST['priority'];
        $Workorder->domain                          = isset($_POST['domain'])?$_POST['domain']:NULL;
        $Workorder->vendor                          = isset($_POST['vendor'])?$_POST['vendor']:NULL;
        $Workorder->assignee_office_id              = $_POST['assigned_id'];
        $Workorder->issuer_id                       = Auth::user()['id'];
        $Workorder->issuer_office_id                = Auth::user()['om_office_id'];
        $Workorder->execution_start                 = ($_POST['execution_start']!="")?$_POST['execution_start']:NULL;
        $Workorder->execution_end                   = ($_POST['execution_end']!="")?$_POST['execution_end']:NULL;
        $Workorder->notes                           = $_POST['notes'];
        $Workorder->created_at                      = \Carbon\Carbon::now();
        $Workorder->updated_at                      = \Carbon\Carbon::now();
        $Workorder->line_manager_approval_required  = isset($_POST['line_manager_approval_required'])?$_POST['line_manager_approval_required']:NULL;
        $Workorder->approver_id                     = isset($_POST['approver_id'])?$_POST['approver_id']:NULL;
        $Workorder->group_approval_required         = isset($_POST['group_approval_required'])?$_POST['group_approval_required']:NULL;
        $Workorder->group_approval_status           = isset($_POST['group_approval_required'])?0:NULL;

        $workorder_template_save = $Workorder->save();
        $Workorder_id = $Workorder->id;

        $user_groups = DB::table('wo_group_users')
                        ->select('user_id','group_id')
                        ->where('group_id',$_POST['group_approval_required'])
                        ->get()->toArray();
        $users=array();
        foreach($user_groups as $key => $value){
            $users[$key]['wo_id'] = $Workorder->id;
            $users[$key]['approver_id'] = NULL;
            $users[$key]['user_id'] = $value->user_id;
            $users[$key]['group_id'] = $value->group_id;
            $users[$key]['approval_status'] = "Pending";
            $users[$key]['created_at'] = \Carbon\Carbon::now();;
            $users[$key]['updated_at'] = \Carbon\Carbon::now();;
        };
        GroupApproval::insert($users);
        $file_info = array();
        if($request->hasfile('filenames'))
        {
            // $name="";
            // $path="";
            // foreach($request->file('filenames') as $file)
            // {
            //     $name = $file->getClientOriginalName();
            //     $path = public_path('uploads');
            //     $file->move(public_path('uploads'), $name);
            // }
            $fileName = $request->filenames->getClientOriginalName();
            $path    = public_path('uploads');
            $request->filenames->move(public_path('uploads'), $fileName);
            $file_info[] = [
                'wo_id'                             => $Workorder_id,
                'name'                              => $fileName,
                'path'                              => $path,
                'uploaded_by'                       => Auth::user()['id'],
                'created_at'                        => \Carbon\Carbon::now(),
                'updated_at'                        => \Carbon\Carbon::now(),
            ];
            wo_file::insert($file_info);
        }
        $workers = array();
        if(isset($_POST["d"])){
            foreach($_POST["d"] as $index =>$item){
                $workers[] = [
                    'template_id'                     => $_POST['template_id'],
                    'subject'                         => $item['sub'],
                    'description'                     => $_POST['description'],
                    'status'                          => $_POST['status'],
                    'priority'                        => $_POST['priority'],
                    'domain'                          => isset($_POST['domain'])?$_POST['domain']:NULL,
                    'vendor'                          => isset($_POST['vendor'])?$_POST['vendor']:NULL,
                    'assignee_office_id'              => $item['prior'],
                    'parent_id'                       => $Workorder_id,
                    'issuer_id'                       => Auth::user()['id'],
                    'issuer_office_id'                => Auth::user()['om_office_id'],
                    'execution_start'                 => ($_POST['execution_start']!="")?$_POST['execution_start']:NULL,
                    'execution_end'                   => ($_POST['execution_end']!="")?$_POST['execution_end']:NULL,
                    'notes'                           => $_POST['notes'],
                    'created_at'                      => \Carbon\Carbon::now(),
                    'updated_at'                      => \Carbon\Carbon::now(),
                    'line_manager_approval_required'  => isset($_POST['line_manager_approval_required'])?$_POST['line_manager_approval_required']:NULL,
                    'approver_id'                     => isset($_POST['approver_id'])?$_POST['approver_id']:NULL,
                    'group_approval_required'         => isset($_POST['group_approval_required'])?$_POST['group_approval_required']:NULL,
                    'group_approval_status'           => isset($_POST['group_approval_required'])?0:NULL,

                ];
            }
        }
        //$Workorder->save();
        //print_r($_POST); exit;
        //Workorder::insert($workers);
        $wo_ids = array();
        foreach($workers as $worker)
        {
            DB::table('workorders')->insert($worker);

            $wo_ids[] = $wo_cid = DB::getPDO()->lastInsertId();
            $file_info[0]['wo_id']= $wo_cid;
            wo_file::insert($file_info);
        }
        //print_r($wo_ids); exit;
        $users1=array();
        foreach($wo_ids as $key1 => $value1){
            foreach($user_groups as $key => $value){
                $users1[$key]['wo_id'] = $value1;
                $users1[$key]['approver_id'] = NULL;
                $users1[$key]['user_id'] = $value->user_id;
                $users1[$key]['group_id'] = $value->group_id;
                $users1[$key]['approval_status'] = "Pending";
                $users1[$key]['created_at'] = \Carbon\Carbon::now();;
                $users1[$key]['updated_at'] = \Carbon\Carbon::now();;
            };
            //print_r($users1); exit;
            GroupApproval::insert($users1);
        }
        //print_r($users1); exit;
        //print_r($users1); exit;
        return redirect('workorder-list')->with('record', 'Workorder Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\workorder  $workorder
     * @return \Illuminate\Http\Response
     */
    public function show(workorder $workorder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\workorder  $workorder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            //$phone = workorder::find($id)->user;
            //rint_r($phone); exit;
    //     $workflow = workflow::find($id);
    //     $workflow_status = workflow_status::where('workflow_id',$id)->get()->toArray();
    //     $status = Status::where('active_status',1)->get()->toArray();
    // //  echo '<pre>';
    //     // print_r($workflow_status); exit;
    //     foreach($workflow_status as $k=>$v){
    //         $selected[$v['from_status']][$v['to_status']]=1;
    //     }
        //$template = Workorder_template::get()->toArray();
        $workorder  =   DB::table('workorders')
        ->leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
        ->leftJoin('user as u1', 'workorders.issuer_id', '=', 'u1.id')
        ->leftJoin('om_office as office', 'workorders.issuer_office_id', '=', 'office.id')
        ->leftJoin('user as u2', 'workorders.approver_id', '=', 'u2.id')
        ->leftJoin('workorder_templates', 'workorders.template_id', '=', 'workorder_templates.id')
        ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
        ->leftJoin('user as u3', 'workorders.updated_by', '=', 'u3.id')
        ->where('workorders.id', '=', $id)
        ->first(['workorders.*','u1.first_name as issuer_name', 'statuses.name as status_name',
        'workorder_templates.name as template_name','om_office.name as office_name'
        ,'office.name as office_name','u2.first_name as approver_name','u3.first_name as updated_name']);
        $workorder = (array) $workorder;
        //print_r($workorder); exit;

        //$status = Status::where('active_status',1)->get()->toArray();
        $workflow =Workorder_template::find($workorder['template_id']);
       //$_POST
        //print_r($workflow['id']); exit;
        //DB::enableQueryLog();
        $status = workflow_status::
        leftJoin('statuses', 'workflow_statuses.to_status', '=', 'statuses.id')
        ->where('workflow_id',$workflow['workflow_id'])
        ->where('from_status','=',$workorder['status'])
        ->get(['workflow_statuses.to_status','statuses.name',])->toArray();
        //dd(DB::getQueryLog());
        //print_r($status); exit;
    //    ->where('active_status',1)
    //     ->where('workflow_id',1)
    //     ->get()->toArray();
        //$offices = DB::table('om_office')->get();
        $offices = Wo_office::leftJoin('om_office', 'wo_offices.office_id', '=', 'om_office.id')
        ->where('template_id',$workorder['template_id'])
        ->get(['om_office.id','om_office.name'])->toArray();
        // $files_info = wo_file::leftJoin('user', 'wo_files.uploaded_by', '=', 'user.id')
        // //->leftJoin('wo_file', 'wo_files.id', '=', 'wo_files.parent_id')
        // ->where('wo_id',$id)
        // ->get(['wo_files.*', 'user.first_name as user_name'])->toArray();
        //print_r($files_info); exit;
        $files_info = DB::select('select *,case when parent_id is null then id else parent_id end as doc_id from wo_files where wo_id = ? order by created_at desc, id desc', [$id]);
        $files_info = array_map(function ($value) {
            return (array)$value;
        }, $files_info);
        $file_types = array_fill_keys(array_unique(array_column($files_info ,'doc_id')),false);
        //print_r($file_types); exit;
        $status_name = array_column($status, 'name');
        $child  =   workorder::leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
                    ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
                    ->where('parent_id',$id)
                    ->get(['workorders.*', 'statuses.name as status_name','om_office.name as office_name'])->toArray();

        //print_r($child); exit;
        return view('workorder/workorder-edit', compact('workorder','status','offices','child','files_info','file_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\workorder  $workorder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, workorder $workorder)
    {
        $Workorder = Workorder::find($_POST["id"]);

        //$Workorder->template_id                     = $_POST['template_id'];
        $Workorder->subject                         = $_POST['subject'];
        $Workorder->description                     = $_POST['description'];
        $Workorder->status                          = $_POST['status'];
        $Workorder->priority                        = $_POST['priority'];
        $Workorder->domain                          = isset($_POST['domain'])?$_POST['domain']:NULL;
        $Workorder->vendor                          = isset($_POST['vendor'])?$_POST['vendor']:NULL;
        $Workorder->assignee_office_id              = $_POST['assigned_id'];
        $Workorder->updated_by                      = Auth::user()['id'];
        //$Workorder->execution_start                 = ($_POST['execution_start']!="")?$_POST['execution_start']:NULL;
        //$Workorder->execution_end                   = ($_POST['execution_end']!="")?$_POST['execution_end']:NULL;
        $Workorder->notes                           = $_POST['notes'];
        $Workorder->updated_at                      = \Carbon\Carbon::now();

        $Workorder->save();
        return redirect()->route('workorder.edit', ['id' => $_POST["id"]]);
        //return redirect('edit-workorder/{$id}')->with('record', 'Status Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\workorder  $workorder
     * @return \Illuminate\Http\Response
     */
    public function destroy(workorder $workorder)
    {
        //
    }

    public function ajaxTemplateFields(){
        $templates = Workorder_template::find($_POST['template_id']);
        $templates['selected_groups'] = Wo_office::leftJoin('om_office', 'wo_offices.office_id', '=', 'om_office.id')
        ->where('template_id',$_POST['template_id'])
        ->get(['om_office.id','om_office.name'])->toArray();
        //print_r($template); exit;
        $templates['statuses'] = workflow_status::
        leftJoin('statuses', 'workflow_statuses.to_status', '=', 'statuses.id')
        ->where('workflow_id',$_POST['workflow_id'])
        ->where('from_status','=',0)
        ->get(['workflow_statuses.to_status','statuses.name',])->toArray();
        //print_r($templates['statuses']); exit;
        return $templates;
        //exit;
        //return response()->json($templates['wo_fields_configs']);
    }
    public function ajaxWorkorderList()
    {
 //       $data_filters = $_POST['data_filters']['string'];
        //$data_filter =array();
        $data_filters = $_POST['data_filters'];
        parse_str($data_filters,$data_filter);
        // echo '<pre>';
        // print_r($data_filter);
        // exit;
        // foreach($data_filters as $key => $value){
        //     if($value['value']!= "" && isset($value['value']))
        //     //print_r($value['value']);
        //     // if($)
        //         $query->where(function ($query) use($value){
        //             $query->where($value['key'],$value['value']);
        //     // });
        // }
        //exit;
        //echo '<pre>';
        //print_r($_POST['data_filters']); exit;
        //$status = (isset($_POST['status']))?$_POST['status']:2;
        //print_r($status);
        //DB::enableQueryLog();
        //$user = auth()->user()->id;
        $workorder = DB::table('workorders')
        ->leftJoin('statuses', 'workorders.status', '=', 'statuses.id')
        ->leftJoin('workorder_templates', 'workorders.template_id', '=', 'workorder_templates.id')
        ->leftJoin('om_office', 'workorders.assignee_office_id', '=', 'om_office.id')
        // ->when($status, function ($query,$status) {
        //     return $query->where('status', $status);
        // })
        // ->where(function ($query) use($data_filters){
        //     foreach($data_filters as $key => $value){
        //         if($data_filters)
        //             $query->where(function ($query) use($data_filters){
        //                 $query->where('status',$data_filters);
        //             });
        //     }
        // })
        ->where(function ($query) use($data_filter){
            foreach($data_filter['integer'] as $key => $value){
                if($value!= ""  && isset($value)){
                    $query->where(function ($query) use($value,$key){
                        $query->where($key,$value);
                    });
                }
            };
            foreach($data_filter['string'] as $key => $value){
                if($value!= ""  && isset($value)){
                    $query->where(function ($query) use($value,$key){
                        $query->where($key,'Like','%'.$value.'%');
                    });
                }
            };
        //exit;
        })

        ->where(function ($query){
            $query->where(function ($query) {
                $query  ->where('line_manager_approval_required',0)
                        ->where('group_approval_required',NULL);
            })->orWhere(function($query){
                $query  ->where('line_manager_approval_required',1)
                        ->where('approver_id','!=',NULL)
                        ->where('group_approval_required',NULL);
            })->orWhere(function($query){
                $query  ->where('line_manager_approval_required',0)
                        ->where('group_approval_required','!=',NULL)
                        ->where('group_approval_status',1);
            })->orWhere(function($query){
                $query  ->where('line_manager_approval_required',1)
                        ->where('group_approval_required','!=',NULL)
                        ->where('group_approval_status',1)
                        ->where('approver_id','!=',NULL);
            });
        })
        ->get(['workorders.*',
        'statuses.name as status_name',
        'workorder_templates.name as template_name',
        'om_office.name as office_name']);
        //dd(DB::getQueryLog());
        // /print_r($workorder); exit;
        return Datatables::of($workorder)

        ->editColumn('priority', function($workorder) {

            if($workorder->priority==1){
                return "<span class=\"badge badge-primary\">High</span>";
            }elseif($workorder->priority==2) {
                return "<span class=\"badge badge-primary\">Medium</span>";
            }elseif($workorder->priority==3) {
                return "<span class=\"badge badge-primary\">Low</span>";
            }else {
                return "<span class=\"badge badge-danger\">Normal</span>";
            }
        })
        ->editColumn('vendor', function($workorder) {

            if($workorder->vendor==1){
                return "<span class=\"badge badge-primary\">Huawei</span>";
            }elseif($workorder->vendor==2) {
                return "<span class=\"badge badge-primary\">ZTE</span>";
            }elseif($workorder->vendor==3) {
                return "<span class=\"badge badge-primary\">Nokia</span>";
            }
        })
        ->editColumn('domain', function($workorder) {

            if($workorder->domain==1){
                return "<span class=\"badge badge-primary\">RAN</span>";
            }elseif($workorder->domain==2) {
                return "<span class=\"badge badge-primary\">Core</span>";
            }elseif($workorder->domain==3) {
                return "<span class=\"badge badge-primary\">Optical</span>";
            }elseif($workorder->domain==4) {
                return "<span class=\"badge badge-primary\">Microwave</span>";
            }elseif($workorder->domain==5) {
                return "<span class=\"badge badge-primary\">Datacom</span>";
            }elseif($workorder->domain==6) {
                return "<span class=\"badge badge-primary\">Power</span>";
            }
        })
        ->editColumn('id', function($workorder) {

            return "<a class=\"text-success font-weight-bold font-italic\" href=\"/edit-workorder/$workorder->id \" >$workorder->id</a>";
        })
        // ->addColumn('action', function ($workorder) {
        //     // if($ticket->booking_status == 0)
        //     // {
        //         return "<div style=\"color:white\">
        //                     <a data-id='.$workorder->id.' href=\"/edit-workflow/".$workorder->id."\" class=\"btn btn-success detail-order\" type=\"button\" >Update</a>
        //                 <div>";
                        //<a data-id='.$workflow->id.' href=\"#\" class=\"btn btn-danger start-process\">Delete</a>
            // }
            // else{
            // return '<div style="color:white">
            //             <a data-id='.$ticket->id.' class="btn btn-primary detail-order" type="button" data-toggle="modal" data-target="#exampleModalCenter" >Detail</a>
            //             <a data-id='.$ticket->id.' href="#" class="btn btn-success active-service">Approved
            //             </a>
            //         <div>';
            // // return 'status';
            // }
        //})
        ->rawColumns(['is_active','id','priority','vendor','domain'])
        ->make(true);
    }
    public function workorder_update_file(Request $request){
        //print_r($_POST); exit;
        $file_info = array();
        $fileName= "";
        $path= "";
        if($request->hasfile('filenames'))
        {
            $fileName = $request->filenames->getClientOriginalName();
            $path    = public_path('uploads');
            $request->filenames->move(public_path('uploads'), $fileName);
            $wo_files = new wo_file;
            //$Workorder->template_id                     = $_POST['template_id'];
            $wo_files->name                                 = $fileName;
            $wo_files->path                                 = $path;
            $wo_files->parent_id                            = $_POST['parent'];
            $wo_files->note                                 = $_POST['note'];
            $wo_files->wo_id                                = $_POST['id'];
            $wo_files->uploaded_by                          = Auth::user()['id'];
            $wo_files->created_at                           = \Carbon\Carbon::now();
            $wo_files->updated_at                           = \Carbon\Carbon::now();
            $wo_files->save();
            return redirect()->route('workorder.edit', ['id' => $_POST["id"]]);
        }
        return redirect()->route('workorder.edit', ['id' => $_POST["id"]]);
    }
}
