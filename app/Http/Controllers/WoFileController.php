<?php

namespace App\Http\Controllers;

use App\Models\wo_file;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WoFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\wo_file  $wo_file
     * @return \Illuminate\Http\Response
     */
    public function show(wo_file $wo_file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\wo_file  $wo_file
     * @return \Illuminate\Http\Response
     */
    public function edit(wo_file $wo_file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\wo_file  $wo_file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, wo_file $wo_file)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\wo_file  $wo_file
     * @return \Illuminate\Http\Response
     */
    public function destroy(wo_file $wo_file)
    {
        //
    }
}
