<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_approvals', function (Blueprint $table) {
            $table->id();
            $table->integer('wo_id');
            $table->integer('group_id');
            $table->integer('approver_id')->nullable();
            $table->integer('user_id');
            $table->string('approval_status');
            $table->string('note')->nullable();
            $table->timestamps();
            $table->foreign('wo_id')->references('id')->on('workorders');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_approvals');
    }
}
