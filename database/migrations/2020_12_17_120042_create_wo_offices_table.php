<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWoOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wo_offices', function (Blueprint $table) {
            $table->id();
            $table->integer('template_id');
            $table->integer('office_id');
            $table->timestamps();
            $table->foreign('template_id')->references('id')->on('workorder_templates');
            $table->foreign('office_id')->references('id')->on('om_office');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wo_offices');
    }
}
