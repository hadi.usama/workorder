<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkorderTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workorder_templates', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('workflow_id');
            $table->integer('approval_group_id')->nullable();
            $table->integer('line_manager_approval');
            $table->time('execution_start')->nullable();
            $table->time('execution_end')->nullable();
            $table->jsonb('wo_fields_configs')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('workflow_id')->references('id')->on('workflows');
            $table->foreign('approval_group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workorder_templates');
    }
}
