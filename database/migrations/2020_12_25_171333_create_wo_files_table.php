<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWoFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wo_files', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('path')->nullable();
            $table->string('note')->nullable();
            $table->integer('wo_id');
            $table->integer('parent_id')->nullable();
            $table->integer('uploaded_by');
            $table->integer('line_manager_approval_required')->nullable();
            $table->integer('group_approval_required')->nullable();
            $table->timestamps();
            $table->foreign('wo_id')->references('id')->on('workorders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wo_files');
    }
}
