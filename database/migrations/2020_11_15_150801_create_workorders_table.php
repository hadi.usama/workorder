<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workorders', function (Blueprint $table) {
            //$table->dropForeign(['template_id']);
            $table->id();
            $table->string('subject');
            $table->string('description')->nullable();
            $table->integer('priority');
            $table->integer('template_id');
            $table->integer('domain')->nullable();
            $table->integer('vendor')->nullable();
            $table->integer('issuer_id')->nullable();
            $table->integer('issuer_office_id')->nullable();
            $table->integer('assignee_id')->nullable();
            $table->integer('assignee_office_id');
            $table->timestamp('execution_start')->nullable();
            $table->timestamp('execution_end')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('status');
            $table->string('notes')->nullable();
            //$table->integer('parent_id')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->integer('line_manager_approval_required')->nullable();
            $table->integer('approver_id')->nullable();
            $table->integer('group_approval_required')->nullable();
            $table->integer('group_approval_status')->nullable();
            $table->foreign('template_id')->references('id')->on('workorder_templates');
            $table->foreign('issuer_id')->references('id')->on('user');
            $table->foreign('assignee_office_id')->references('id')->on('om_office');
            //$table->foreign('template_id')->references('id')->on('workorder_templates');
            $table->foreign('group_approval_required')->references('id')->on('groups');
            //$table->foreign('parent_id')->references('id')->on('workorders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workorders');
    }
}
