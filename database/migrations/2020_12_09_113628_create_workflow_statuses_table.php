<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_statuses', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('workflow_id');
            $table->foreign('workflow_id')->references('id')->on('workflows');
            $table->tinyInteger('from_status');
            $table->tinyInteger('to_status');
            $table->tinyInteger('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_statuses');
    }
}
