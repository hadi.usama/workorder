<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\workflowController;
use App\Http\Controllers\WorkorderTemplateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home1', function () {
    return view('home/index');
});

// Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
// Route::get('/minor', [HomeController::class, 'minor'])->name("minor");
// //Route::get('/home', [HomeController::class, 'index'])->name('home');

// //ticket routes starts
// //Route::get('/ticket', [HomeController::class, 'minor'])->name("ticket");

// Route::get('/tickets', function () {
//     return view('tickets.show');
// })->middleware('auth');
// Route::get('/ticket/add', [TicketController::class, 'ticketAdd'])->name("ticket.add")->middleware('auth');

// Route::post('/ticket_save', [TicketController::class, 'store'])->name("ticket.save");
// Route::get('/Ajaxticket', [TicketController::class, 'ajaxListTicket'])->name("ajax.ticket.list");
// //ticket routes Ends

// //process routes start
// Route::get('/processes', function () {
//     return view('process.show');
// })->middleware('auth');
// Route::get('/process/add', [ProcessController::class, 'processAdd'])->name("process.add")->middleware('auth');
// Route::post('/process/start', [ProcessController::class, 'processStart'])->name("process.start")->middleware('auth');
// //process routes ends

// //API
// Route::get('/start-process', [CamundaController::class, 'startProcess'])->name("start.process");

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', function () {
        return view('home/index');
    });
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //workorder start
    // Route::get('workorder-list', function () {
    //     return view('workorder/workorder-list');
    // });
    Route::get('workorder-list',[App\Http\Controllers\WorkorderController::class, 'index'])->name('workorder.list');

    Route::get('workorder-add',[App\Http\Controllers\WorkorderController::class, 'create'])->name('workorder.add');
    Route::post('/workorder-save', [App\Http\Controllers\WorkorderController::class, 'store'])->name("workorder.save");

    Route::get('/edit-workorder/{id}', [App\Http\Controllers\WorkorderController::class, 'edit'])->name("workorder.edit");
    Route::post('/workorder_update', [App\Http\Controllers\WorkorderController::class, 'update'])->name('workorder.update');
    Route::post('/workorder_update_file', [App\Http\Controllers\WorkorderController::class, 'workorder_update_file'])->name('workorder.update.file');

    Route::post('/ajaxWorkorderRecords', [App\Http\Controllers\WorkorderController::class, 'ajaxWorkorderList'])->name("ajax.workorder.list");
    Route::post('/ajaxTemplateFields', [App\Http\Controllers\WorkorderController::class, 'ajaxTemplateFields'])->name("template.fields");

    //workorder End

    //workorder template start
    Route::get('workorder-temp-list', function () {
        return view('workorder-template/workorder-list');
    });
    Route::get('workorder-temp-add', [App\Http\Controllers\WorkorderTemplateController::class, 'create'])->name('workorder-template.add');
    Route::post('/workorder-temp-save', [App\Http\Controllers\WorkorderTemplateController::class, 'store'])->name('workorder-template.save');

    Route::get('/ajaxWorkorderTempRecords', [App\Http\Controllers\WorkorderTemplateController::class, 'ajaxWokorderTempList'])->name("ajax.workorder.temp.list");
    //workorder template end

    //workflow Engine Start
    Route::get('workflow-list', function () {
        return view('workflow-engine/workflow-engine-list');
    });

    Route::get('/workflow-engine-add', [App\Http\Controllers\WorkflowController::class, 'create'])->name('workflow.add');
    Route::post('/workflow_save', [App\Http\Controllers\WorkflowController::class, 'store'])->name('workflow.save');
    Route::post('/workflow_update', [App\Http\Controllers\WorkflowController::class, 'update'])->name('workflow_update');


    Route::get('/ajaxWorkflowRecords', [App\Http\Controllers\WorkflowController::class, 'ajaxWorkflowList'])->name("ajax.workflow.list");
    Route::get('/edit-workflow/{id}', [App\Http\Controllers\WorkflowController::class, 'edit'])->name("workflow.edit");
    //workflow Engine End

    //Status Start
    Route::get('status-list', function () {
        return view('status/status-list');
    });
    Route::get('status-add', function () {
        return view('status/status-add');
    });
    Route::post('/status_save', [App\Http\Controllers\StatusController::class, 'store'])->name('status.save');
    Route::post('/update_save', [App\Http\Controllers\StatusController::class, 'update'])->name('update.save');

    Route::get('/ajaxStatusRecords', [App\Http\Controllers\StatusController::class, 'ajaxStatusList'])->name('ajax.status.list');
    // Route::get('/ajaxStatusRecords', [StatusController::class, 'ajaxStatusRecords'])->name("ajax.status.records");
    Route::get('/edit-status/{id}', [App\Http\Controllers\StatusController::class, 'edit'])->name("status.edit");
    //Status End


    //group Start
    Route::get('group-list', function () {
        return view('group/group-list');
    });
    Route::get('group-add', [App\Http\Controllers\GroupController::class, 'create']);

    Route::post('/group_save', [App\Http\Controllers\GroupController::class, 'store'])->name('group.save');
    Route::post('/update_save_group', [App\Http\Controllers\GroupController::class, 'update'])->name('update.save');

    Route::get('/ajaxGroupRecords', [App\Http\Controllers\GroupController::class, 'ajaxGroupList'])->name("ajax.group.list");
    Route::get('/edit-group/{id}', [App\Http\Controllers\GroupController::class, 'edit'])->name("group.edit");
    //group End

    Route::prefix('employee')
        ->as('employee.')
        ->group(function() {
            Route::get('home', 'Home\EmployeeHomeController@index')->name('home');Route::namespace('Auth\Login')
        ->group(function() {

        //Route::post('login', 'EmployeeController@login')->name('login');
        Route::post('logout', 'EmployeeController@logout')->name('logout');
        });
    });
    //group Approval Start
    Route::get('group-approval-list', [App\Http\Controllers\GroupApprovalController::class, 'index'])->name("group.workorder.list");

    Route::get('/edit-group-workorder/{id}', [App\Http\Controllers\GroupApprovalController::class, 'edit'])->name("group.workorder.edit");

    Route::post('/ajaxGroupWorkorderRecords', [App\Http\Controllers\GroupApprovalController::class, 'ajaxGroupWorkorderList'])->name("ajax.group.workorder.list");

    Route::post('/user_approval', [App\Http\Controllers\GroupApprovalController::class, 'user_approval'])->name("user.approval");
    Route::post('/manager_approval', [App\Http\Controllers\GroupApprovalController::class, 'manager_approval'])->name("manager.approval");
    //group Approval end
    //Route::get('login', [App\Http\Controllers\Auth\Login\EmployeeController::class,'showLoginForm'])->name('login');
    //Route::post('login', [App\Http\Controllers\Auth\Login\EmployeeController::class,'login'])->name('employee.login');
    Route::get('logout' , [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
});
Route::post('login' , [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::get('login' , [App\Http\Controllers\Auth\LoginController::class, 'loginRedirect'])->name('login.redirect');
