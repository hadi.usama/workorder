@extends('layouts-theme.app')

@section('title', 'Main page')
@push('styles')
<style>
    table{
    width:100%;
}
thead{
    background:#CCC;
    font-weight:bold;
}
td{
    padding:0.25em 0em;
    border:0px solid #AAA; */
}
td>.col-form-label{
    padding:0.25em 0em;
}
td>input{
    padding:0.25em 0em;
}
.table>tbody>tr>td,
.table>tbody>tr>th {
  border-top: none;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>
@endpush

@section('content')
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Issue workorder</h4>
    </div>
</div>
@if($errors->any())
    <div class="alert alert-danger m-2">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content pl-5 pr-5">
                    <form method="POST" action="{{ route('workorder.save') }}" class="container" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Template Name:*</label>
                            <div class="col-sm-10">
                                <select id="template" class="form-control" name="template_id" required>
                                    <option value=""> Select Template </option>
                                    @foreach($templates as $row)
                                        <option data-wfid="{{ $row['workflow_id'] }}" value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div id="fields" > <!--style="display: none;" -->
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Subject / Reference:*</label>
                                <div class="col-sm-10"><input type="text" name="subject" class="form-control" required></div>
                            </div>
                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea type="text" name="description" class="form-control" ></textarea>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Status:*</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="status_box" name="status" required>
                                        <option value=""> Select Status </option>
                                        {{-- @foreach($status as $row)
                                            <option value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                        @endforeach --}}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Priority:*</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="priority" required>
                                        <option value=""> Select Priority </option>
                                        <option value="1"> High </option>
                                        <option value="2"> Medium </option>
                                        <option value="3"> Low </option>
                                        <option value="4"> Normal </option>
                                    </select>
                                </div>
                            </div>
                            <?php ?>
                            {{-- <div class="form-group row"><label class="col-sm-2 col-form-label">Domain:*</label>
                                <div class="col-sm-10"><select class="form-control" name="ticket_type" required>
                                    <option value="general">General</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Vendor:*</label>
                                <div class="col-sm-10"><select class="form-control" name="ticket_type" required>
                                    <option value="general">General</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div id="d_fields">

                            </div>
                            <div class="form-group row"><label class="col-sm-2 col-form-label">Issue to:*</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="assigned_id" id="select_box" required>
                                        <option value="">Select Unit/Office: </option>
                                        {{-- @foreach($offices as $row)
                                            <option value="{{ $row->id }}"> {{ $row->name }} </option>
                                        @endforeach --}}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" ><label class="col-sm-2 col-form-label">Execution: </label>
                                <div class="col-sm-2">
                                    <select class="form-control p-0 pl-1" id="execution" required>
                                        <option value="1" selected>On a Prticular Date</option>
                                        <option value="2">As soon as possible</option>
                                    </select>
                                </div>

                                <label class="col-sm-2 col-form-label date">Execution Start Date: </label>
                                    <div class="input-group col-sm-2 p-0 date" data-autoclose="true">
                                        <input type="text" class="form-control datepicker"   name="execution_start">
                                        <span class="input-group-addon date">
                                            <span class="fa fa-clock-o date"></span>
                                        </span>
                                    </div>
                                    {{-- <input class="col-sm-2 form-control date" type="time" name="execution_start"> --}}
                                {{-- </div>--}}
                                <label class="col-sm-2 col-form-label date">Execution End Date:</label>
                                <div class="input-group col-sm-2 p-0 date" data-autoclose="true">
                                    <input type="text" class="form-control datepicker"   name="execution_end">
                                    <span class="input-group-addon date">
                                        <span class="fa fa-clock-o date"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group  row"><label class="col-sm-2 col-form-label">Notes:</label>
                                <div class="col-sm-10">
                                    <textarea type="text" name="notes" class="form-control" required></textarea>
                                </div>
                            </div>
                            {{-- <div class="form-group row"><label class="col-sm-2 col-md-2 col-form-label">Related Workorders: </label>
                                <div class="col-sm-2"><select class="form-control" name="ticket_type" required>
                                    <option value="general">General</option>
                                    </select>
                                </div>
                                <div class="col-sm-8"><select class="form-control" name="ticket_type" required>
                                    <option value="general">General</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="form-group row" ><label class="col-sm-2 col-form-label" >Attachments :</label>
                                <div class="col-sm-10" >
                                    <div class=" col-sm-12" >
                                        <input id="logo" type="file" name="filenames" class="custom-file-input form-control" multiple>
                                        <label for="logo" class="custom-file-label">Choose file...</label>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-solid"></div>
                            <div class="form-group row"><label class="col-sm-3 col-form-label">Line Manager Approval Required:</label>
                                <div class="col-sm-4"><select class="form-control" id="line_manager" name="line_manager_approval_required" readonly>
                                    <option value="">Select</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-sm-3 col-form-label">CAB Approval Required:</label>
                                <div class="col-sm-4"><select class="form-control" id="approval_required" name="group_approval_required" readonly>
                                    @foreach($groups as $row)
                                        <option value="{{ $row->id }}"> {{ $row->group_name }} </option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-solid"></div>
                            {{-- <div class="form-group  row mb-1">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <label class="col-sm-5 col-form-label">Issue child WOs for the same WO:</label>
                                        <div class="col-md-7"><input type="text" placeholder=".col-md-2" class="form-control"></div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <label class="col-sm-2 col-form-label">Issue to:*</label>
                                        <div class="col-md-10"><input type="text" placeholder=".col-md-2" class="form-control"></div>
                                    </div>

                                </div>
                            </div> --}}

                            <h3>Issue child WOs for the same WO:</h3>

                            <table class="table">
                                <tbody id="tbody">
                                    <tr>
                                        {{-- <td><label class="col-sm-12 col-form-label">Issue child WOs for the same WO:</label></td> --}}
                                        {{-- <td><div class="col-md-12 p-0"><input type="text" placeholder=".col-md-2" class="form-control"></div></td>
                                        <td><label class="col-sm-12 col-form-label">Issue to:*</label></td>
                                        <td><div class="col-md-12 p-0"><input type="text" placeholder=".col-md-2" class="form-control"></div></td>
                                        <td><div class="col-md-12 p-0"><input type="text" placeholder=".col-md-2" class="form-control"></div></td> --}}
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mb-5">
                                <div class="col-sm-12">
                                <div class="form-group  row mb-1 pull-right">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" id="addBtn" type="button">Clone</button>
                                        {{-- <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" id="addBtn" type="submit">Clone</button> --}}
                                    </div>
                                </div>
                                </div>
                            </div>
                            {{-- <button class="btn btn-md btn-primary" id="addBtn" type="button">
                                Add new Row
                            </button> --}}
                            <div class="row">
                                <div class="col-sm-12">
                                <div class="form-group row text-right">
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1">Submit</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$( "#template" ).change(function() {
if(this.value){
    var template_id = this.value;
    var workflow_id = $(this).find(':selected').attr('data-wfid');
    //console.log(workflow_id);
    $.ajax({
        url : "{{ route('template.fields') }}",
        type : 'POST',
        data : { "_token": "{{ csrf_token() }}",'template_id' : template_id,'workflow_id' : workflow_id},
        dataType:'json',
        success : function(data) {
            fields = "";
            var data_fields= JSON.parse(data.wo_fields_configs);
            //console.log(data_fields);
            for (i in data_fields) {
                if(data_fields[i]=="domain"){
                    //alert(data_fields[i]);
                    fields += " <div class=\"form-group row\"><label class=\"col-sm-2 col-form-label\">Domain:*</label>\
                                    <div class=\"col-sm-10\"><select class=\"form-control\" name=\"domain\" required>\
                                        <option value=\"\">Select Domain</option>\
                                        <option value=\"1\">RAN</option>\
                                        <option value=\"2\">Core</option>\
                                        <option value=\"3\">Optical</option>\
                                        <option value=\"4\">Microwave</option>\
                                        <option value=\"5\">Datacom</option>\
                                        <option value=\"6\">Power</option>\
                                        </select>\
                                    </div>\
                                </div>";
                }else{
                    //alert(data_fields[i]);
                    fields += " <div class=\"form-group row\"><label class=\"col-sm-2 col-form-label\">Vendor:*</label>\
                                    <div class=\"col-sm-10\"><select class=\"form-control\" name=\"vendor\" required>\
                                        <option value=\"\">Selelct Vendor</option>\
                                        <option value=\"1\">Huawei</option>\
                                        <option value=\"2\">ZTE</option>\
                                        <option value=\"3\">Nokia</option>\
                                        </select>\
                                    </div>\
                                </div>";
                }
            }
            $("#d_fields" ).html(fields);
            $("#fields").css("display", "block");
            if(data.line_manager_approval==1||data.line_manager_approval==0){
                $("#line_manager").val(data.line_manager_approval);
                $('#line_manager').attr('readonly', true);
                $("#line_manager").attr("required", false);
            }else if(data.line_manager_approval==2){
                $('#line_manager').attr('readonly', false);
                $("#line_manager").attr("required", true);
                $("#line_manager").val("");
            }

            $("#approval_required").val(data.approval_group_id);
            var selelcted_groups = data.selected_groups;
            selelcted_groups.forEach(function(selelcted_groups) {
                $("#select_box").append("<option value='"+selelcted_groups.id+"'>" + selelcted_groups.name + "</option>");
            });
            var statuses = data.statuses;
            statuses.forEach(function(statuses) {
                $("#status_box").append("<option value='"+statuses.to_status+"'>" + statuses.name + "</option>");
            });
        },
        error : function(request,error)
        {
            alert("Request: "+JSON.stringify(request));
        }
    });
}else{
    //alert('hi');
    $("#fields").css("display", "none");
};
});
$("#execution").change(function() {
    //alert('hi'); exit;
    //var value = $('#execution').val();
    //alert(this.value);
    if(this.value==1){
        $(".date").css("display", "block");
        $('.date').attr("disabled", false);
    }else{
        $(".date").css("display", "none");
        //$('input:time').attr("disabled", 'disabled');
        $('.date').attr("disabled", 'disabled');
    }
});
//$('.clockpicker').clockpicker();
var rowIdx = 0;

      // jQuery button click event to add a row
      $('#addBtn').on('click', function () {
        if(rowIdx == 0){
            if($('#tbody').find('tr').length> 0){
                $('#tbody').html('');
            }
        }


        // Adding a row inside the tbody.
        $('#tbody').append(`
        <tr id="R${++rowIdx}">
            <td><label class="col-sm-12 col-form-label">Subject:</label></td>
            <td><div class="col-md-12 p-0"><input type="text" name="d[${rowIdx}][sub]" class="form-control" required></div></td>
            <td><label class="col-sm-12 col-form-label">Issue to:*</label></td>
            <td><div class="col-sm-10">
                                    <select class="form-control d_options" id="selectbox${rowIdx}" name="d[${rowIdx}][prior]" required>
                                    </select>
                                </div></td>
            <td class="text-right">
                <button class="btn btn-danger remove"
                    type="button">Remove
                </button>
            </td>
        </tr>`);
        ////alert("selectbox"+rowIdx);   original
        //<td id="selectbox${rowIdx}"></td>
        // $( "#select_box" ).clone().appendTo("#selectbox"+rowIdx);
        var $options = $("#select_box > option").clone();
        $("#selectbox"+rowIdx).append($options);
      });

      // jQuery button click event to remove a row.
      $('#tbody').on('click', '.remove', function () {

        // Getting all the rows next to the row
        // containing the clicked button
        var child = $(this).closest('tr').nextAll();

        // Iterating across all the rows
        // obtained to change the index
        child.each(function () {

          // Getting <tr> id.
          var id = $(this).attr('id');

          // Getting the <p> inside the .row-index class.
          var idx = $(this).children('.row-index').children('p');

          // Gets the row number from <tr> id.
          var dig = parseInt(id.substring(1));

          // Modifying row index.
          idx.html(`Row ${dig - 1}`);

          // Modifying row id.
          $(this).attr('id', `R${dig - 1}`);
        });

        // Removing the current row.
        $(this).closest('tr').remove();

        // Decreasing total number of rows by 1.
        rowIdx--;
      });
// $('.date').attr("disabled", 'disabled');
$(".datepicker").datetimepicker();
$('.custom-file-input').on('change', function() {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
});
</script>
@endpush
