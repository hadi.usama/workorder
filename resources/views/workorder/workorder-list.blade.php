@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
@if (session('record'))
    <div class="alert alert-success m-2">
        {{ session('record') }}
    </div>
@endif
<div class="row wrapper border bg-white mr-2 ml-2 mt-2">
    <div class="col-lg-12 p-0">
        <div class="ibox">
            <div class="ibox-title">
                <h3>Workorder List</h3>
                <div class="ibox-tools text-white">
                    <a class="btn btn-md btn-success text-white float-right"  href="{{ url('/workorder-add') }}"><strong style="color: white;">Issue Workorder</strong></a>
                    {{-- <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a> --}}
                </div>
            </div>
            <div class="ibox-content">
                {{-- <div class="row wrapper border bg-white m-2"> --}}
                    {{-- <div class="col-lg-12"> --}}
                    <form id="search_filters" >
                        {{-- @csrf --}}
                        <div class="row mb-1">
                            <div class="col-sm-2 pl-0">
                                <input class="form-control"  type="number" placeholder="Search by ID" name="integer[workorders.id]"/>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="integer[status]" required>
                                    <option value="">Select Status</option>
                                    @foreach($status as $row)
                                        <option value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2 pl-0">
                                <input class="form-control"  type="text" placeholder="Search by Subject" name="string[subject]"/>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="integer[template_id]" required>
                                    <option value="">Select WO Type</option>
                                    @foreach($templates as $row)
                                        <option value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="integer[priority]" required>
                                    <option value=""> Select Priority </option>
                                    <option value="1"> High </option>
                                    <option value="2"> Medium </option>
                                    <option value="3"> Low </option>
                                    <option value="4"> Normal </option>
                                </select>
                            </div>
                            <div class="col-sm-2 pl-0">
                                <input class="form-control"  type="text" placeholder="Search by Desc." name="string[workorders.description]"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2 pl-0">
                                <select class="form-control" name="integer[domain]" required>
                                <option value="">Select Domain</option>
                                <option value="1">RAN</option>
                                <option value="2">Core</option>
                                <option value="3">Optical</option>
                                <option value="4">Microwave</option>
                                <option value="5">Datacom</option>
                                <option value="6">Power</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control" name="integer[vendor]" required>
                                    <option value="">Selelct Vendor</option>
                                    <option value="1">Huawei</option>
                                    <option value="2">ZTE</option>
                                    <option value="3">Nokia</option>
                                </select>
                            </div>
                            <div class="col-sm-2 pl-0">
                                <select class="form-control" name="integer[assignee_office_id]" required>
                                    <option value="">Select Office</option>
                                    @foreach($offices as $row)
                                        <option value="{{ $row->id }}"> {{ $row->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                            {{-- <input  type="number" name="status"/> --}}
                            {{-- <input  type="text" name="subject"/>
                            <input  type="text" name="priority"/>
                            <input  type="text" name="template_id"/>
                            <input  type="text" name="domain"/>
                            <input  type="text" name="vendor"/>
                            <input  type="number" name="assignee_office_id"/> --}}
                            {{-- <input  id="search_subject" type="date" name="vendor"/> --}}
                        {{-- </div> --}}
                        <button class="btn btn-md btn-success text-white float-right"  id="search_button"  ><strong style="color: white;">Search</strong></button>
                    </form>
                    {{-- </div> --}}
                {{-- </div> --}}
            </div>
        </div>
    </div>
</div>
{{-- <div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <input  id="search_status" type="text" />
        <input  id="search_subject" type="text" />
    </div>
</div> --}}
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox ">
            {{-- <div class="ibox-title">
                <h5>Workorder(s)</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div> --}}
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTables-example text-center" >
                    <thead>
                    <tr>
                        <th>WO ID</th>
                        <th>Status</th>
                        <th>Subject</th>
                        <th>WO Type</th>
                        <th>Priority</th>
                        <th>Description</th>
                        <th>Domain</th>
                        <th>Vendor</th>
                        <th>Receiver Unit</th>
                        <th>Issue DateTime</th>
                        <th>Execution Start</th>
                        <th>Execution End Date</th>
                        <th>Last Update Time</th>
                        {{-- <th>Activation Status</th> --}}
                    </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    //var  data_filters =   $('#search_filters').serializeArray();
    // $('#search_button').click(function () {
//     var  data_filters =   $('#search_filters').serializeArray();
//     $('#dataTables-example').dataTable().fnDestroy()
// });
// $( "#search_filters" ).submit(function( event ) {
//   //alert( "Handler for .submit() called." );
//   //event.preventDefault();
//     var  data_filters =   $('#search_filters').serializeArray();
//     table.ajax.reload(null, false)
//     return false;
// });
//console.log($dd);
$('#search_button').click(function () {
    //data_filters =   $('#search_filters').serializeArray();
    table.ajax.reload(null, false)
    return false;
    //alert(data_filters);
});
// $( "#search_filters" ).submit(function( event ) {
//var  data_filters =   $('#search_filters').serializeArray();

var table = $('#dataTables-example').DataTable({
    pageLength: 10,
    responsive: true,
    searching: false,
    dom: '<"html5buttons"B>lTfgtp<"bottom"i>',
    buttons: [
        //{extend: 'copy'},
        {extend: 'csv'},
        //{extend: 'excel', title: 'ExampleFile'},
        //{extend: 'pdf', title: 'ExampleFile'},
        // {extend: 'print',
        //     customize: function (win){
        //         $(win.document.body).addClass('white-bg');
        //         $(win.document.body).css('font-size', '10px');
        //         $(win.document.body).find('table')
        //         .addClass('compact')
        //         .css('font-size', 'inherit');
        //     }
        // }
    ],
    processing: true,
    serverSide: true,
    ajax:
    {
        "url": "{{ route("ajax.workorder.list") }}",
        "type": "POST",
        "data" : //{ "_token": "{{ csrf_token() }}",'data_filters':data_filters}
                    function(d) {
                    d._token = "{{ csrf_token() }}";
                    d.data_filters = $('#search_filters').serialize();
                },
    },
    columns: [
        { data: 'id', name: 'id' },
        { data: 'status_name', name: 'status_name' },
        { data: 'subject', name: 'subject' },
        { data: 'template_name', name: 'template_name' },
        { data: 'priority', name: 'priority' },
        { data: 'description', name: 'description' },
        { data: 'domain', name: 'domain' },
        { data: 'vendor', name: 'vendor' },
        { data: 'office_name', name: 'office_name' },
        { data: 'created_at', name: 'created_at' },
        { data: 'execution_start', name: 'execution_start' },
        { data: 'execution_end', name: 'execution_end' },
        { data: 'updated_at', name: 'updated_at' },
    ],
});
// table.ajax.reload(null, false)
//return false;
// ;
// function SubmitForm(form) {
            //console.log($(form).serialize());
        //     $('form#search_filters').on('submit',function(e) {
        //         //e.preventDefault();
        //         var form = $(this);
        //     $.ajax({

        //         type: 'POST',
        //         url: "{{ route("ajax.workorder.list") }}",  //form.action,
        //         data: form.serialize(),

        //         success: function (data) {

        //                 //console.log(data.data);
        //                 //Popup.dialog('close');
        //                 //$('#dataTables-example').DataTable().reload();
        //                 //table.ajax.reload();
        //                 table.ajax.reload();
        //             //
        //         }
        //     });
        //     return false;
        // });
// $('#search_subject').on('change', function(){

//     table
//     .column(2)
//     .search(this.value)
//     .draw();

// });
// var versionNo = $.fn.dataTable.version;
//     alert(versionNo);
//$( "#search_filters" ).trigger( "submit" );
</script>
@endpush
