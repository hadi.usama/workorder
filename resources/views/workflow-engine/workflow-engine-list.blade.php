@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
@if (session('record'))
    <div class="alert alert-success m-2">
        {{ session('record') }}
    </div>
@endif
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Workflow configurations</h4>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-md btn-success text-white float-right " href="{{ url('workflow-engine-add') }}"><strong>Create a Workflow</strong></a>
    </div>
</div>
{{-- <input type="text"  id="search-date"> --}}
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Workflow(s)</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTables-example text-center" >
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Activation Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
 var table = $('#dataTables-example').DataTable({
    pageLength: 10,
    responsive: true,
    // "bPaginate": true,
    //     "processing": true,
    //     "bServerSide": true,
    dom: '<"html5buttons"B>lTfgitp',
    buttons: [
        //{extend: 'copy'},
        {extend: 'csv'},
        //{extend: 'excel', title: 'ExampleFile'},
        //{extend: 'pdf', title: 'ExampleFile'},
        // {extend: 'print',
        //     customize: function (win){
        //         $(win.document.body).addClass('white-bg');
        //         $(win.document.body).css('font-size', '10px');
        //         $(win.document.body).find('table')
        //         .addClass('compact')
        //         .css('font-size', 'inherit');
        //     }
        // }
    ],
    processing: true,
        //serverSide: true,
        ajax: '{{ route("ajax.workflow.list") }}',
    columns: [
    { data: 'id', name: 'id' },
    { data: 'name', name: 'name' },
    { data: 'description', name: 'description' },
    { data: 'is_active', name: 'Active Status' },
    { data: 'action', name: 'action' },
    ],
});
//var table = $('#example').DataTable();

  //$('#search-date').datepicker({"dateFormat":"yy/mm/dd"});
//   $('#search-date').on('change', function(){

//     table
//     .column(1)
//     .search(this.value)
//     .draw();

//   });
</script>
@endpush
@endsection
