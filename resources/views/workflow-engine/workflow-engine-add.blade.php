@extends('layouts-theme.app')

@section('title', 'Main page')

@push('styles')
{{-- Enter css here --}}
<style>
.highlight {
    background-color:#bbffbb;
}
.highlight-disabled {
    background-color:#939393;
}
.icon-color{
    color: #18a689;
}
</style>
@endpush

@section('content')
<?php //print_r($status); //exit; ?>
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4 id="check-css">Create Workflow</h4>
    </div>
</div>
@if($errors->any())
    <div class="alert alert-danger m-2">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <form id="workflow_form" method="POST" action="{{ route('workflow.save') }}">
                    @csrf
                    <input type="hidden" name="fields" value="{{ $status_id }}">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Workflow name:</label>
                        <div class="col-sm-10"><input type="text" name="name" class="form-control" required></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col-sm-12 col-sm-offset-2">
                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button> --}}
                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                        </div>
                    </div>
                    <div class="hr-line-solid"></div>
                    <div class="panel-body">
                        <table id="data-table" class="table table-striped table-bordered nowrap text-center transitions-always" width="100%">
                            <thead>
                                <tr>
                                <th><button class="btn btn-link pr-1" onclick="toggleCheckboxesBySelector('table.transitions-always input[type=checkbox]'); return false;"><span  class="fa fa-check icon-color"></span></button><span>Current Status</span></th>
                                <th colspan="<?php echo count($status); ?>">New Statuses Allowed</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td class="pl-0"><button class="btn btn-link pr-1 pb-0 pt-0" onclick="toggleCheckboxesBySelector('table.transitions-always input[type=checkbox]'); return false;"><span  class="fa fa-check icon-color"></span></button><span class="font-14 font-bold"><?php echo implode('</td><td class="pl-0"><button class="btn btn-link pr-1 pb-0 pt-0" onclick="toggleCheckboxesBySelector(\'table.transitions-always input[type=checkbox]\'); return false;"><span  class="fa fa-check icon-color"></span></button><span class="font-14 font-bold">', $status_name); ?></span></td>
                                </tr>
                                <tr>
                                    <td><label class="label label-primary">New WO</label></td>
                                        @php
                                            $y=0;
                                        @endphp
                                    @foreach ($status as $index =>$item)
                                            {{-- @if($y != 0) --}}
                                                <td>
                                                    <input type="checkbox" class="checkbox-size" id="checkboxes" name="status[0][{{$item['id']}}]" value="1">
                                                </td>
                                            {{-- @else
                                                <td class="highlight-disabled">
                                                    <input type="checkbox" class="checkbox-size" id="checkboxes" name="status[0][{{$item['id']}}]" value="1" onclick="return false;" readonly checked>
                                                </td>
                                            @endif --}}
                                            @php
                                                $y++;
                                            @endphp
                                        @endforeach
                                </tr>
                                @foreach ($status as $index1 =>$item1)
                                    <tr>
                                        <td>
                                            <button class="btn btn-link pr-1 pb-0 pt-0" onclick="toggleCheckboxesBySelector('table.transitions-always input[type=checkbox]'); return false;"><span  class="fa fa-check icon-color"></span></button> <span class="font-14 font-bold">{{$item1['name']}}</span>
                                        </td>
                                        {{-- <td> --}}
                                        @php
                                            $x=0;
                                        @endphp
                                        @foreach ($status as $index =>$item)
                                            @if($x != $index1)
                                                <td>
                                                    <input type="checkbox" class="checkbox-size" id="checkboxes" name="status[{{$item1['id']}}][{{$item['id']}}]" value="1">
                                                </td>
                                            @else
                                                <td class="highlight-disabled">
                                                    <input type="checkbox" class="checkbox-size" id="checkboxes" name="status[{{$item1['id']}}][{{$item['id']}}]" value="1" onclick="return false;" readonly checked>
                                                </td>
                                            @endif
                                            @php
                                                $x++;
                                            @endphp
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
    $('td input').change(function() {
        $(this).closest('td').toggleClass("highlight", this.checked);
    });
});
function toggleCheckboxesBySelector(selector) {
    var all_checked = true;
    $(selector).each(function(index) {
        if (!$(this).is(':checked')) { all_checked = false; }
    });
    $(selector).prop('checked', !all_checked);
}
</script>
@endpush
