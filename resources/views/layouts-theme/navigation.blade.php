<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs text-center">
                                <strong class="font-bold">{{Auth::user()->first_name}}</strong>
                            {{-- </span> <span class="text-muted text-xs block">Example menu <b class="caret"></b></span> --}}
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="">
                <a href="{{ url('/') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            {{-- <li class="">
                <a href="{{ url('/minor') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Minor view</span> </a>
            </li>
            <li class="">
                <a href="{{ url('/tickets') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Tickets</span> </a>
            </li>
            <li class="">
                <a href="{{ url('/processes') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Processes</span> </a>
            </li> --}}
            <li class="">
                <a href="{{ url('/workorder-list') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Workorders</span> </a>
            </li>
            <li class="">
                <a href="{{ url('/group-approval-list') }}"><i class="fa fa-th-large"></i> <span class="nav-label">WO Approval</span> </a>
            </li>
            <li class="">
                <a href="{{ url('/workorder-temp-list') }}"><i class="fa fa-th-large"></i> <span class="nav-label">WO Templates</span> </a>
            </li>
            <li>
                <a href="#"><i class="fa fa-random"></i> <span class="nav-label">Workflow Engine</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ url('/workflow-list') }}">Workflow Engine</a></li>
                    {{-- <li><a href="#">WO Orchestration</a></li> --}}
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/status-list') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Status</span></a>
            </li>
            {{-- <li>
                <a href="#"><i class="fa fa-random"></i> <span class="nav-label">User Management</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ url('/group-list') }}">Group Management</a></li>
                </ul>
            </li> --}}
            <li>
                <a href="{{ url('/group-list') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Group Management</span></a>
            </li>
        </ul>

    </div>
</nav>
