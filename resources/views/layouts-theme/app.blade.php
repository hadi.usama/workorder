<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INX-WOS - @yield('title') </title>


    {{-- <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" /> --}}
    <link rel="stylesheet" href="{!! asset('theme/css/style.css') !!}" />

    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('theme/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    {{-- jquery-ui --}}
    <link href="{{ asset('theme/css/plugins/jQueryUI/jquery-ui.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('theme/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/js/plugins/wickedpicker/stylesheets/wickedpicker.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/plugins/jquery-ui-datetimepicker/jquery-ui-timepicker-addon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-css.css') }}" rel="stylesheet">


</head>
<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts-theme.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts-theme.topnavbar')
            @stack('styles')
            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts-theme.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
 <!-- Mainly scripts -->
 <script src="{{ asset('theme/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('theme/js/popper.min.js') }}"></script>
    <script src="{{ asset('theme/js/bootstrap.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('theme/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/flot/jquery.flot.pie.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('theme/js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('theme/js/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('theme/js/inspinia.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/pace/pace.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('theme/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- GITTER -->
    <script src="{{ asset('theme/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('theme/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('theme/js/demo/sparkline-demo.js') }}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('theme/js/plugins/chartJs/Chart.min.js') }}"></script>

    <!-- Toastr -->
    <script src="{{ asset('theme/js/plugins/toastr/toastr.min.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('theme/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/wickedpicker/src/wickedpicker.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/clockpicker/clockpicker.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/jquery-ui-datetimepicker/jquery-ui-sliderAccess.js') }}"></script>
    <script src="{{ asset('theme/js/plugins/jquery-ui-datetimepicker/jquery-ui-timepicker-addon.js') }}"></script>

    @stack('scripts')
    <script>

    </script>
@section('scripts')
@show

</body>
</html>
