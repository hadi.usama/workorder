@extends('layouts-theme.app')

@section('title', 'Main page')

@push('styles')
{{-- Enter css here --}}
<style>
.highlight {
    background-color:#bbffbb;
}
.highlight-disabled {
    background-color:#939393;
}
.icon-color{
    color: #18a689;
}
</style>
@endpush

@section('content')
@php
    //print_r($workorder);
@endphp
<div class="row wrapper border bg-white mr-2 ml-2 p-1">
    <div class="col-lg-10">
        <h4>Workorder: {{ $workorder['id'] }}</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-9">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                {{-- <a href="#" class="btn btn-white btn-xs float-right">Edit project</a> --}}
                                <h2>Workorder Detail</h2>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Current Status:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1"><span class="label label-primary">{{ $workorder['status_name'] }}</span></dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Issuer:</dt> </div>
                                <div class="col-sm-8 text-sm-left"><dd class="mb-1">{{ $workorder['issuer_name'] }}</dd> </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>Issuer Office/Unit:</dt> </div>
                                <div class="col-sm-8 text-sm-left"> <dd class="mb-1">  {{ $workorder['office_name'] }}</dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"> <dt>Line Manager Approval:</dt></div>
                                <div class="col-sm-8 text-sm-left"> <dd class="mb-1"> {{ $workorder['approver_name'] }} </dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"><dt>CAB Approval:</dt> </div>
                                <div class="col-sm-8 text-sm-left"> <dd class="mb-1"><a href="#" class="text-navy"></a> </dd></div>
                                {{-- Not required/ Approved / Pending / Partially Approved --}}
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right"> <dt>Receiver:</dt></div>
                                <div class="col-sm-8 text-sm-left"> <dd class="mb-1">------</dd></div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Receiver Office / Unit:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{ $workorder['office_name'] }}</dd>
                                </div>
                            </dl>
                        </div>
                        <div class="col-lg-6" id="cluster_info">

                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Date of Issuance:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{ $workorder['created_at'] }}</dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Date of Approval:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1"> 10.07.2014 23:36:57</dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Execution Start Date:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{ $workorder['execution_start'] ?? 'As Soon As Possible' }}</dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Execution End Date:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{ $workorder['execution_end']  ?? 'As Soon As Possible' }}</dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Last Update:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{ $workorder['updated_at'] }}</dd>
                                </div>
                            </dl>
                            <dl class="row mb-0">
                                <div class="col-sm-4 text-sm-right">
                                    <dt>Last Updated by:</dt>
                                </div>
                                <div class="col-sm-8 text-sm-left">
                                    <dd class="mb-1">{{$workorder['updated_name'] ?? 'N/A'}}</dd>
                                </div>
                            </dl>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-lg-12">
                            <dl class="row mb-0">
                                <div class="col-sm-2 text-sm-right">
                                    <dt>Completed:</dt>
                                </div>
                                <div class="col-sm-10 text-sm-left">
                                    <dd>
                                        <div class="progress m-b-1">
                                            <div style="width: 60%;" class="progress-bar progress-bar-striped progress-bar-animated"></div>
                                        </div>
                                        <small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
                                    </dd>
                                </div>
                            </dl>
                        </div>
                    </div> --}}
                    <div class="row m-t-sm">
                        <div class="col-lg-12">
                        <div class="panel blank-panel">
                        <div class="panel-heading">
                            <div class="panel-options">
                                <ul class="nav nav-tabs">
                                    {{-- <li><a class="nav-link" href="#tab-1" data-toggle="tab">History</a></li> --}}
                                    <li><a class="nav-link active" href="#tab-2" data-toggle="tab">Approvals</a></li>
                                    <li><a class="nav-link" href="#tab-3" data-toggle="tab">WO Info</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">

                        <div class="tab-content">
                        {{-- <div class="tab-pane active" id="tab-1">
                            <div class="feed-activity-list">
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/a2.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">2h ago</small>
                                        <strong>Mark Johnson</strong> posted message on <strong>Monica Smith</strong> site. <br>
                                        <small class="text-muted">Today 2:10 pm - 12.06.2014</small>
                                        <div class="well">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                            Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                        </div>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/a3.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">2h ago</small>
                                        <strong>Janet Rosowski</strong> add 1 photo on <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">2 days ago at 8:30am</small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/a4.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right text-navy">5h ago</small>
                                        <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                        <div class="actions">
                                            <a href=""  class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> Like </a>
                                            <a href=""  class="btn btn-xs btn-white"><i class="fa fa-heart"></i> Love</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/a5.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">2h ago</small>
                                        <strong>Kim Smith</strong> posted message on <strong>Monica Smith</strong> site. <br>
                                        <small class="text-muted">Yesterday 5:20 pm - 12.06.2014</small>
                                        <div class="well">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                            Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                        </div>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/profile.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">23h ago</small>
                                        <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                        <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                    </div>
                                </div>
                                <div class="feed-element">
                                    <a href="#" class="float-left">
                                        <img alt="image" class="rounded-circle" src="/theme/img/a7.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">46h ago</small>
                                        <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </div>
                            </div>

                        </div> --}}
                        <div class="tab-pane active" id="tab-2">
                            @if(Auth::user()->user_role_id==14 && $workorder['issuer_office_id']==Auth::user()->om_office_id && $workorder['line_manager_approval_required']==1 && $workorder['line_manager_approval_required']==1 && $workorder['approver_id']==NULL)
                                <button class="btn btn-md btn-danger pull-right" data-toggle="modal" href="#line-modal-form">Approve Work Order </button>
                            @elseif(Auth::user()->user_role_id==14 && $workorder['issuer_office_id']==Auth::user()->om_office_id)
                                <button class="btn btn-md btn-primary pull-right disabled">Approved by Line Manager</button>
                            @endif
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Approval Group Member</th>
                                    <th>Phone Number</th>
                                    <th>Approval Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($group_members as $key => $row)
                                        <tr>
                                            <td>
                                                {{ $row['first_name'] }}
                                            </td>
                                            <td>
                                                {{ $row['mobile_no'] }}
                                            </td>
                                            <td>
                                                {{ $row['approval_status'] }}
                                            </td>
                                            <td>
                                                @if($row['approval_status']=='Pending' && Auth::user()->id==$row['user_id'])
                                                    <button class="btn btn-md btn-info" data-toggle="modal" href="#modal-form">Approve Workorder</button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content pr-0 pl-0">
                                            <form method="POST"  class="container">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $workorder['id'] }}">
                                                {{-- <div class="form-group  row"><label class="col-sm-2 col-form-label">Template Name:*</label>
                                                    <div class="col-sm-10">
                                                        <select id="template" class="form-control" name="template_id" required>
                                                            <option value=""> Select Template </option>
                                                            @foreach($templates as $row)
                                                                <option value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div> --}}
                                                <div id="fields">
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Subject / Reference:*</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="subject" class="form-control" value="{{ $workorder['subject'] }}" disabled>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" name="description" class="form-control" disabled>{{ $workorder['description'] }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Status:*</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="status" disabled>
                                                                <option value="" > Select Status </option>
                                                                @foreach($status as $row)
                                                                    <option value="{{ $row['id'] }}" @if(  $row['id'] == $workorder['status']) {{ 'selected' }}@else{{ '' }}@endif> {{ $row['name'] }} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Priority:*</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="priority" disabled>
                                                                <option value=""> Select Priority </option>
                                                                <option value="1" @if($workorder['priority'] == 1) {{ 'selected' }}@else{{ '' }}@endif> High </option>
                                                                <option value="2" @if($workorder['priority'] == 2) {{ 'selected' }}@else{{ '' }}@endif> Medium </option>
                                                                <option value="3" @if($workorder['priority'] == 3) {{ 'selected' }}@else{{ '' }}@endif> Low </option>
                                                                <option value="4" @if($workorder['priority'] == 4) {{ 'selected' }}@else{{ '' }}@endif> Normal </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @if($workorder['domain'])
                                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Domain:*</label>
                                                        <div class="col-sm-10"><select class="form-control" name="domain" disabled>
                                                            <option value="">Select Domain</option>
                                                            <option value="1" @if($workorder['domain'] == 1) {{ 'selected' }}@else{{ '' }}@endif>RAN</option>
                                                            <option value="2" @if($workorder['domain'] == 2) {{ 'selected' }}@else{{ '' }}@endif>Core</option>
                                                            <option value="3" @if($workorder['domain'] == 3) {{ 'selected' }}@else{{ '' }}@endif>Optical</option>
                                                            <option value="4" @if($workorder['domain'] == 4) {{ 'selected' }}@else{{ '' }}@endif>Microwave</option>
                                                            <option value="5" @if($workorder['domain'] == 5) {{ 'selected' }}@else{{ '' }}@endif>Datacom</option>
                                                            <option value="6" @if($workorder['domain'] == 6) {{ 'selected' }}@else{{ '' }}@endif>Power</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if($workorder['vendor'])
                                                        <div class="form-group row"><label class="col-sm-2 col-form-label">Vendor:*</label>
                                                            <div class="col-sm-10"><select class="form-control" name="vendor" disabled>
                                                                <option value="">Selelct Vendor</option>
                                                                <option value="1" @if($workorder['vendor'] == 1) {{ 'selected' }}@else{{ '' }}@endif>Huawei</option>
                                                                <option value="2" @if($workorder['vendor'] == 2) {{ 'selected' }}@else{{ '' }}@endif>ZTE</option>
                                                                <option value="3" @if($workorder['vendor'] == 3) {{ 'selected' }}@else{{ '' }}@endif>Nokia</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    {{-- <div class="form-group row"><label class="col-sm-2 col-form-label">Vendor:*</label>
                                                        <div class="col-sm-10"><select class="form-control" name="ticket_type" required>
                                                            <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                    </div>  --}}
                                                    <div id="d_fields">

                                                    </div>
                                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Issue to:*</label>
                                                        <div class="col-sm-10"><select class="form-control" name="assigned_id" disabled>
                                                            <option value="">Select Unit/Office: </option>
                                                            @foreach($offices as $row)
                                                                <option value="{{ $row->id }}"  @if(  $row->id == $workorder['assignee_office_id']) {{ 'selected' }}@else{{ '' }}@endif> {{ $row->name }} </option>
                                                            @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" ><label class="col-sm-2 col-form-label">Execution: </label>
                                                        <div class="col-sm-2">
                                                            <select class="form-control" id="execution" disabled>
                                                                <option value="1" selected>On a Prticular Date</option>
                                                                <option value="2">As soon as possible</option>
                                                            </select>
                                                        </div>

                                                        <label class="col-sm-2 col-form-label date">Execution Start Date: </label>
                                                        {{-- <div class="col-sm-2"> --}}
                                                        {{-- <div class="input-group col-sm-2 clockpicker date" data-autoclose="true">
                                                            <input type="text" class="form-control" value="00:00"  name="execution_start">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-clock-o"></span>
                                                            </span>
                                                        </div> --}}
                                                            <div class="input-group col-sm-2 p-0 date" data-autoclose="true">
                                                                <input type="text date" class="form-control datepicker date" value="{{ $workorder['execution_start'] }}"   name="execution_start" disabled>
                                                                <span class="input-group-addon date">
                                                                    <span class="fa fa-clock-o date"></span>
                                                                </span>
                                                            </div>
                                                            {{-- <input class="col-sm-2 form-control date" type="time" name="execution_start"> --}}
                                                        {{-- </div>--}}
                                                        <label class="col-sm-2 col-form-label date">Execution End Date:</label>
                                                        {{-- <div class="col-sm-2">
                                                            <input class="form-control date" type="time" name="execution_end">
                                                        </div> --}}
                                                        {{-- <div class="input-group col-sm-2 clockpicker date" data-autoclose="true">
                                                            <input type="text" class="form-control" value="00:00"  name="execution_end">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-clock-o"></span>
                                                            </span>
                                                        </div> --}}
                                                        <div class="input-group col-sm-2 p-0 date" data-autoclose="true">
                                                            <input type="text" class="form-control datepicker date" value="{{ $workorder['execution_end'] }}"  name="execution_end" disabled>
                                                            <span class="input-group-addon date">
                                                                <span class="fa fa-clock-o date"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Notes:</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" name="notes" class="form-control" disabled>{{ $workorder['notes'] }}</textarea>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="form-group row"><label class="col-sm-2 col-md-2 col-form-label">Related Workorders: </label>
                                                        <div class="col-sm-2"><select class="form-control" name="ticket_type" required>
                                                            <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-8"><select class="form-control" name="ticket_type" required>
                                                            <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    {{-- <div class="form-group row" ><label class="col-sm-2 col-form-label" >Attachments :</label>
                                                        <div class="col-sm-10" >
                                                            <div class=" col-sm-12 custom-file" >
                                                                <input id="logo" type="file" class="custom-file-input form-control">
                                                                <label for="logo" class="custom-file-label">Choose file...</label>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="hr-line-solid"></div>
                                                    <div class="form-group row"><label class="col-sm-3 col-form-label">Line Manager Approval Required:</label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control" name="ticket_type" disabled>
                                                                <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-sm-3 col-form-label">CAB Approval Required:</label>
                                                        <div class="col-sm-4"><select class="form-control" name="ticket_type" disabled>
                                                            <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="hr-line-solid"></div>
                                                    {{-- <div class="form-group row text-right">
                                                        <div class="col-sm-12 col-sm-offset-2">
                                                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button>
                                                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                                                        </div>
                                                    </div> --}}
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                        </div>

                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="wrapper wrapper-content project-manager">
            <h3 class="font-weight-bold">Workorder Template:</h3>
            <p>
                {{ $workorder['template_name'] }}
            </p>
            <h3 class="font-weight-bold">Subject / Reference:</h3>
            <p>{{ $workorder['subject'] }}</p>
            <h3 class="font-weight-bold">Description:</h3>
            <p>{{ $workorder['description'] }}</p>
            <div class="row pb-2 pt-1">
                <div class="col-sm-4 text-sm-left"><dt>Priority:</dt> </div>
                <div class="col-sm-8 text-sm-left">
                    <dd class="mb-1">
                        <span class="label label-primary">
                            @if ($workorder['priority']==1)
                                High
                            @elseif ($workorder['priority']==2)
                                Medium
                            @elseif ($workorder['priority']==3)
                                Low
                            @elseif ($workorder['priority']==4)
                                Normal
                            @endif
                        </span>
                    </dd>
                </div>
            </div>
            <div class="row pb-2">
                <div class="col-sm-4 text-sm-left"><dt>Domain:</dt> </div>
                <div class="col-sm-8 text-sm-left">
                    @if($workorder['domain'])
                        <dd class="mb-1">
                            <span class="label label-primary">
                                @if ($workorder['domain']==1)
                                    RAN
                                @elseif ($workorder['domain']==2)
                                    Core
                                @elseif ($workorder['domain']==3)
                                    Optical
                                @elseif ($workorder['domain']==4)
                                    Microwave
                                @elseif ($workorder['domain']==5)
                                    Datacom
                                @elseif ($workorder['domain']==6)
                                    Power
                                @endif
                            </span>
                        </dd>
                    @endif
                </div>
            </div>
            <div class="row pb-2">
                <div class="col-sm-4 text-sm-left"><dt>Vendor:</dt> </div>
                <div class="col-sm-8 text-sm-left">
                    @if($workorder['vendor'])
                        <dd class="mb-1">
                            <span class="label label-primary">
                                @if ($workorder['vendor']==1)
                                    Huawei
                                @elseif ($workorder['vendor']==2)
                                    ZTE
                                @elseif ($workorder['vendor']==3)
                                    Nokia
                                @endif
                            </span>
                        </dd>
                    @endif
                </div>
            </div>
                <h3 class="font-weight-bold">Files :</h3>
            {{-- <div class="input-group" data-autoclose="true">
                <input type="text" class="form-control" id="datepicker" value="{{ $workorder['execution_start'] }}"  name="execution_start">
                <span class="input-group-addon">
                    <span class="fa fa-clock-o"></span>
                </span>
            </div> --}}
            {{-- <p class="small font-bold">
                <span><i class="fa fa-circle text-warning"></i> High priority</span>
            </p>
            <h5>Project tag</h5>
            <ul class="tag-list" style="padding: 0">
                <li><a href=""><i class="fa fa-tag"></i> Zender</a></li>
                <li><a href=""><i class="fa fa-tag"></i> Lorem ipsum</a></li>
                <li><a href=""><i class="fa fa-tag"></i> Passages</a></li>
                <li><a href=""><i class="fa fa-tag"></i> Variations</a></li>
            </ul>
            <h5>Project files</h5>
            <ul class="list-unstyled project-files">
                <li><a href=""><i class="fa fa-file"></i> Project_document.docx</a></li>
                <li><a href=""><i class="fa fa-file-picture-o"></i> Logo_zender_company.jpg</a></li>
                <li><a href=""><i class="fa fa-stack-exchange"></i> Email_from_Alex.mln</a></li>
                <li><a href=""><i class="fa fa-file"></i> Contract_20_11_2014.docx</a></li>
            </ul>
            <div class="text-center m-t-md">
                <a href="#" class="btn btn-xs btn-primary">Add files</a>
                <a href="#" class="btn btn-xs btn-primary">Report contact</a>

            </div> --}}
        </div>
    </div>
</div>
<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-sm-12 text-center"><h2 class="m3-t-none m-b">Approve Workorder</h2>
                    </div>
                </div>
                <div class="col-lg-12 p-0 grey-bg">
                    <div class="ibox border-bottom mb-2">
                        <div class="ibox-content">
                            <form method="POST" action="{{ route('user.approval') }}" enctype=multipart/form-data>
                                @csrf
                                {{-- <div class="form-group row"><label class="col-lg-5 col-form-label">Upload revised WO:</label>
                                    <input type="hidden" name="id" value="{{ $workorder['id'] }}">
                                    <input type="hidden" id="data-name" name="name" value="">
                                    <input type="hidden" id="data-parent" name="parent" value="">
                                    <div class="col-lg-7"><input type="file" name="filenames" placeholder="File" class="form-control">
                                    </div>
                                </div> --}}
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="wo_id" value="{{ $workorder['id'] }}">
                                {{-- <input type="hidden" id="data-name" name="name" value=""> --}}
                                {{-- <input type="hidden" id="data-parent" name="parent" value=""> --}}
                                    <h3 class="text-center">{{ Auth::user()->first_name }}</h3>
                                    <h4 class="text-center">( {{ Auth::user()->email }} )</h4>

                                {{-- <div class="form-group row"><label class="col-lg-5 col-form-label">Line Manager Approval Required:</label>
                                    <div class="col-lg-7"><input type="email" placeholder="Email" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row"><label class="col-lg-5 col-form-label">CAB Approval Required:</label>
                                    <div class="col-lg-7"><input type="email" placeholder="Email" class="form-control">
                                    </div>
                                </div> --}}
                                    <div class="col-lg-12 p-0">
                                        <textarea id="w3review" name="note" class="form-control" rows="4" cols="55">Note(s)</textarea>
                                    </div>
                                </div>
                    </div>
                    <div class="form-group row float-right mr-2">
                        <button class="btn btn-md btn-danger mr-2"  data-dismiss="modal">Cancel</button>
                        {{-- <button class="btn btn-md btn-success mr-2 disabled" >Reject</button> --}}
                        <button class="btn btn-md btn-primary"  type="submit">Approve Workorder</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="line-modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-sm-12 text-center"><h2 class="m3-t-none m-b">Line Manager Approval</h2>
                    </div>
                </div>
                <div class="col-lg-12 p-0 grey-bg">
                    <div class="ibox border-bottom mb-2">
                        <div class="ibox-content">
                            <form method="POST" action="{{ route('manager.approval') }}" enctype=multipart/form-data>
                                @csrf
                                {{-- <div class="form-group row"><label class="col-lg-5 col-form-label">Upload revised WO:</label>
                                    <input type="hidden" name="id" value="{{ $workorder['id'] }}">
                                    <input type="hidden" id="data-name" name="name" value="">
                                    <input type="hidden" id="data-parent" name="parent" value="">
                                    <div class="col-lg-7"><input type="file" name="filenames" placeholder="File" class="form-control">
                                    </div>
                                </div> --}}
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="wo_id" value="{{ $workorder['id'] }}">
                                {{-- <input type="hidden" id="data-name" name="name" value=""> --}}
                                {{-- <input type="hidden" id="data-parent" name="parent" value=""> --}}
                                    <h3 class="text-center">{{ Auth::user()->first_name }}</h3>
                                    <h4 class="text-center">( {{ Auth::user()->email }} )</h4>

                                {{-- <div class="form-group row"><label class="col-lg-5 col-form-label">Line Manager Approval Required:</label>
                                    <div class="col-lg-7"><input type="email" placeholder="Email" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row"><label class="col-lg-5 col-form-label">CAB Approval Required:</label>
                                    <div class="col-lg-7"><input type="email" placeholder="Email" class="form-control">
                                    </div>
                                </div> --}}
                                    <div class="col-lg-12 p-0">
                                        <textarea id="w3review" name="note" class="form-control" rows="4" cols="55">Note(s)</textarea>
                                    </div>
                                </div>
                    </div>
                    <div class="form-group row float-right mr-2">
                        <button class="btn btn-md btn-danger mr-2"  data-dismiss="modal">Cancel</button>
                        {{-- <button class="btn btn-md btn-success mr-2 disabled" >Reject</button> --}}
                        <button class="btn btn-md btn-primary"  type="submit">Approve Workorder</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    // $(function() {
    //     $('td input').change(function() {
    //         $(this).closest('td').toggleClass("highlight", this.checked);
    //     });
    // });
    function toggleCheckboxesBySelector(selector) {
        var all_checked = true;
        $(selector).each(function(index) {
            if (!$(this).is(':checked')) { all_checked = false; }
        });
        $(selector).prop('checked', !all_checked);
    }
    $("#execution").change(function() {
        //alert('hi'); exit;
        //var value = $('#execution').val();
        //alert(this.value);
        if(this.value==1){
            $(".date").css("display", "block");
            $('.date').attr("disabled", false);
        }else{
            $(".date").css("display", "none");
            //$('input:time').attr("disabled", 'disabled');
            $('.date').attr("disabled", 'disabled');
        }
    });
    //$('.clockpicker').clockpicker();
    $(".datepicker").datetimepicker();
</script>
@endpush
