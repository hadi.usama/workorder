@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
<?php //print_r($group); exit; ?>
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Update Group</h4>
    </div>
</div>
@if($errors->any())
    <div class="alert alert-danger m-2">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
            <form method="POST" action="{{ url('/update_save_group') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $group->id }}">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Group name:</label>
                    <div class="col-sm-10"><input type="text" name="group_name" class="form-control" value="{{ $group->group_name }}"  readonly></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="group_description" class="form-control" >{{ $group->group_description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Group Members:</label>
                        <div class="col-sm-10">
                            <select data-placeholder="Choose Offices/Units..." class="chosen-select" multiple  tabindex="4" name="group_users[]">
                                <option value="">Select</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}" <?php if(array_search($user->id, $results) !== false) {echo 'selected';} ?> > {{ $user->first_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col-sm-12 col-sm-offset-2">
                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button> --}}
                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$('.chosen-select').chosen({width: "100%"});
</script>
@endpush
