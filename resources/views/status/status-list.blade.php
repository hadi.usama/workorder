@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
@if (session('record'))
    <div class="alert alert-success m-2">
        {{ session('record') }}
    </div>
@endif
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>WO Statuses</h4>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-md btn-success text-white float-right " href="{{ url('/status-add') }}"><strong>Add Status</strong></a>
    </div>
</div>
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Statuses</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTables-example text-center" >
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status Type</th>
                        <th>Activation Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    // $(document).ready(function(){
        $('#dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                //{ extend: 'copy'},
                {extend: 'csv'},
                //{extend: 'excel', title: 'ExampleFile'},
                //{extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                // customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //                 .addClass('compact')
                //                 .css('font-size', 'inherit');
                // }
                // }
            ],
            processing: true,
                //serverSide: true,
                ajax: '{{ route("ajax.status.list") }}',

                columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'status_type', name: 'Status Type' },
                { data: 'active_status', name: 'Active Status' },
                { data: 'action', name: 'action' },
                ],
            });
            // $('body').on('click','.start-process',function(){
            //     $.ajax({
            //         type: 'get',
            //         url:'',
            //         success:function(data) {
            //             $("#msg").html(data.msg);
            //         }
            //         }).always(function (data) {
            //             $('#service_datatable').DataTable().draw(false);
            //         });
            // });
        // });
</script>
@endpush
@endsection
