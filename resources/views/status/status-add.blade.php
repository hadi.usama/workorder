@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Add New Status</h4>
    </div>
</div>
@if($errors->any())
    <div class="alert alert-danger m-2">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
            <form method="POST" action="{{ url('/status_save') }}">
                    @csrf
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Status name:</label>
                        <div class="col-sm-10"><input type="text" name="name" class="form-control" required></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Status Type:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="status_type" >
                                <option value="Open">Open</option>
                                <option value="Close">Close</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Active Status:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="active_status" >
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col-sm-12 col-sm-offset-2">
                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button> --}}
                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
