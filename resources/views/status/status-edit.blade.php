@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Update Status</h4>
    </div>
</div>
<div class="row wrapper border bg-white m-2">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
            <form method="POST" action="{{ url('/update_save') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $status->id }}">
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Status name:</label>
                    <div class="col-sm-10"><input type="text" name="name" class="form-control" value="{{ $status->name }}" required></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" name="description" class="form-control" >{{ $status->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Status Type:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="status_type" required>
                                <option value="Open"    @if($status->status_type == "Open") {{ 'selected' }}@else{{ '' }}@endif>Open</option>
                                <option value="Close"   @if($status->status_type == "Close") {{ 'selected' }}@else{{ '' }}@endif>Close</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Active Status:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="active_status" required>
                                <option value="1"   @if($status->active_status == 1) {{ 'selected' }}@else{{ '' }}@endif>Active</option>
                                <option value="0"   @if($status->active_status == 0) {{ 'selected' }}@else{{ '' }}@endif>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col-sm-12 col-sm-offset-2">
                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button> --}}
                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
