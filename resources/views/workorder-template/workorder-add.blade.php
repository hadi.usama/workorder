@extends('layouts-theme.app')

@section('title', 'Main page')

@section('content')
<div class="row wrapper border bg-white m-2 p-1">
    <div class="col-lg-10">
        <h4>Create workorder Template:</h4>
    </div>
    <div class="col-lg-2">
        {{-- <a class="btn btn-md btn-primary text-white float-right " href="{{ url('/workorder-temp-add') }}"><strong>Save WO Workorder</strong></a> --}}
    </div>
</div>
{{-- @php
    print_r($workflow);
@endphp --}}
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-1">General Settings</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-2">Advanced Template Configurations</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox">
                                            <div class="ibox-content pl-5 pr-5">
                                                <form method="POST" action="{{ route('workorder-template.save') }}">
                                                    @csrf
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Template Name:*</label>
                                                        <div class="col-sm-10"><input type="text" name="name" class="form-control" ></div>
                                                    </div>
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Line Manager Approval Required:</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="manager_approval" >
                                                                <option value="">Select</option>
                                                                <option value="1">Yes</option>
                                                                <option value="0">No</option>
                                                                <option value="2">To be decided during WO issuance</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">CAB Approval Required:</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="cab_approval" >
                                                                <option value="">Select</option>
                                                                @foreach($groups as $row)
                                                                    <option value="{{ $row->id }}"> {{ $row->group_name }} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-sm-2 col-form-label font-weight-bold">Allowed Window of Execution:</label>
                                                        <label class="col-sm-2 col-form-label">Execution Start Time: </label>
                                                        {{-- <div class="col-sm-2"> --}}
                                                            <div class="input-group col-sm-2 clockpicker" data-autoclose="true">
                                                                <input type="text" class="form-control"  name="start_time">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-clock-o"></span>
                                                                </span>
                                                            </div>
                                                        {{-- </div>--}}
                                                        <label class="col-sm-2  col-form-label">Execution End Time:</label>
                                                        <div class="input-group col-sm-2 clockpicker" data-autoclose="true">
                                                            <input type="text" class="form-control"  name="end_time">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-clock-o"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Allowed Offices/Units for Execution:</label>
                                                        <div class="col-sm-10">
                                                            <select data-placeholder="Choose Offices/Unit..." class="chosen-select" multiple  tabindex="4" name="unit[]">
                                                                <option value="">Select</option>
                                                                @foreach($offices as $row)
                                                                    <option value="{{ $row->id }}"> {{ $row->name }} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    {{-- <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Dependent Workorders:</label>
                                                        <div class="col-sm-10"><select class="form-control" name="ticket_type" required>
                                                            <option value="general">General</option>
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    <div class="form-group  row"><label class="col-sm-2 col-form-label font-weight-bold">Workflow:</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="workflow_id" required>
                                                                <option value="">Select Workflow</option>
                                                                @foreach($workflow as $row)
                                                                    <option value="{{ $row['id'] }}"> {{ $row['name'] }} </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading font-weight-bold">Workorder Form Field Configurations:
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="alert alert-info">Note: Only below selected fields will be shown when issuing a workorder against this template.</div>
                                                                    <div class="col-lg-12">
                                                                        <table class="table table-striped">
                                                                            <thead>
                                                                            <tr class="bg-dark text-white">
                                                                                <th>Standard Fields</th>
                                                                                <th>Selection</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td class="font-bold">Subject:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="subject" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Description:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="description" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Priority:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="priority" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">WO Type:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="wo_type" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Domain:</td>
                                                                                <td>
                                                                                    {{-- <input type="hidden" name="domain" value="0" /> --}}
                                                                                    <input type="checkbox" name="fields[]" class="switch_domain" value="domain" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Vendor:</td>
                                                                                <td>
                                                                                    {{-- <input type="hidden" name="vendor" value="vendor" /> --}}
                                                                                    <input type="checkbox" name="fields[]" class="switch_vendor" value="vendor" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Issuer:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="issuer" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Issuer Office/Unit:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="issuer_unit" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Receiver:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="receiver" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Receiver Unit:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="receiver_unit" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Issue DateTime:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="issue_starttime" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Execution Start DateTime:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="execution_starttime" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Execution End Date Time:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="execution_endtime" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Last Update Time:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="last_time" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="font-bold">Status:</td>
                                                                                <td>
                                                                                    Available in all forms
                                                                                    {{-- <input type="hidden" name="status" value="1"> --}}
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row text-right">
                                                        <div class="col-sm-12 col-sm-offset-2">
                                                            {{-- <button class="btn btn-info btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Save as Draft</button> --}}
                                                            <button class="btn btn-success btn-md pl-5 pr-5 pt-1 pb-1" type="submit">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" id="tab-2" class="tab-pane">
                        <div class="panel-body">
                             <strong>Advanced Template Configurations !</strong>
                            {{--<p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>
                            <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$('.chosen-select').chosen({width: "100%"});
// var options = {
//     twentyFour: true, //Display 24 hour format, defaults to false
//     showSeconds: true,
//     title: 'HH:MM:SS',
// };
// $('.timepicker').wickedpicker(options);
$('.clockpicker').clockpicker();
var elem = document.querySelector('.switch_domain');
var switchery = new Switchery(elem, { color: '#1AB394' });
var elem = document.querySelector('.switch_vendor');
var switchery = new Switchery(elem, { color: '#1AB394' });
</script>
@endpush
